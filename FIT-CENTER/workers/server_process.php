<?php
	
	
	require 'table/conexion.php';
	
	/* Nombre de La Tabla */
	$sTabla = "domicilios";
	
	/* Array que contiene los nombres de las columnas de la tabla*/
	$aColumnas = array( 'id',
                       'FID',
                       'LINE1',
                       'LINE2',
                       'COLONIA',
                       'CONICIPALITY',
                       'CITY',
                       'STATE',
                       'POSTAL_SECTION',
                       'POSTAL_LAST2',
                       'POSTAL_PLUS5',
                       'PHONE_NUMBER',
                       'PHONE_NUM',
                       'PHONE_LAST5',
                       'PHONE_EXT',
                       'FAX_AREA_CODE',
                       'FAX_PHONE_NUMBER',
                       'FAX_PHONE_NUM',
                       'FAX_PHONE_LAST5',
                       'SPECIAL_INDIC',
                       'USE_CNT',
                       'LAST_USED_DT',
                       'RESIDENCE_DT',
                       'REPT_MEMBER_KOB',
                       'REPT_MEMBER',
                       'RPTED_DT',
                       'TYPE',
                       'SOUNDEX_PATERNAL',
                       'SOUNDEX_MATERNAL',
                       'SOUNDEX_ADDT_SURNM',
                       'FIRST_INITIAL',
                       'PATNL_PATNL_CNT',
                       'PATNL_MATNL_CNT',
                       'MATNL_PATNL_CNT',
                       'MATNL_MATNL_CNT',
                       'COUNTRY_CODE',
                       'EXTRA'
                      );


	
	/* columna indexada */
	$sIndexColumn = "id";
	
	// Paginacion
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".$_GET['iDisplayStart'].", ".$_GET['iDisplayLength'];
	}
	
	
	//Ordenacion
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
		{
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
			{
				$sOrder .= $aColumnas[ intval( $_GET['iSortCol_'.$i] ) ]."
				".$_GET['sSortDir_'.$i] .", ";
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	
	//Filtracion
	$sWhere = "";
	if ( $_GET['sSearch'] != "" )
	{
		$sWhere = "WHERE (";
		for ( $i=0 ; $i<count($aColumnas) ; $i++ )
		{
			$sWhere .= $aColumnas[$i]." LIKE '%".$_GET['sSearch']."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
	}
	
	// Filtrado de columna individual 
	for ( $i=0 ; $i<count($aColumnas) ; $i++ )
	{
		if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			if ( $sWhere == "" )
			{
				$sWhere = "WHERE ";
			}
			else
			{
				$sWhere .= " AND ";
			}
			$sWhere .= $aColumnas[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
		}
	}
	
	
	//Obtener datos para mostrar SQL queries
	$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumnas))."
	FROM   $sTabla
	$sWhere
	$sOrder
	$sLimit
	";
	$rResult = $mysqli->query($sQuery);
	
	/* Data set length after filtering */
	$sQuery = "
	SELECT FOUND_ROWS()
	";
	$rResultFilterTotal = $mysqli->query($sQuery);
	$aResultFilterTotal = $rResultFilterTotal->fetch_array();
	$iFilteredTotal = $aResultFilterTotal[0];
	
	/* Total data set length */
	$sQuery = "
	SELECT COUNT(".$sIndexColumn.")
	FROM   $sTabla
	";
	$rResultTotal = $mysqli->query($sQuery);
	$aResultTotal = $rResultTotal->fetch_array();
	$iTotal = $aResultTotal[0];
	
	/*
		* Output
	*/
	$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
	);
	
	while ( $aRow = $rResult->fetch_array())
	{
		$row = array();
		for ( $i=0 ; $i<count($aColumnas) ; $i++ )
		{
			if ( $aColumnas[$i] == "version" )
			{
				/* Special output formatting for 'version' column */
				$row[] = ($aRow[ $aColumnas[$i] ]=="0") ? '-' : $aRow[ $aColumnas[$i] ];
			}
			else if ( $aColumnas[$i] != ' ' )
			{
				/* General output */
				$row[] = $aRow[ $aColumnas[$i] ];
			}
		}
		//para llamar hacer funcion  class='btn btn-success btn-sm' data-toggle='modal'>
        //$row[] ="<td><a href='#edit_?id=".$aRow['id']."'><span class='glyphicon glyphicon-edit'></span> Editar</a></td>";
       	//para llamar hacer funcion   class='btn btn-danger btn-sm' data-toggle='modal''>
        //$row[] ="<td><a href='#delete_?id=".$aRow['id']."'><span class='glyphicon glyphicon-trash'></span> Eliminar</a></td>";
          
                                                    
   //  $row[] = "<td><a href='archivo.php?id=".$aRow['id']."'><CENTER><span class='glyphicon glyphicon-eye-open'></span></CENTER></a></td>";       
   	$row[] = "<td><a href='actualizar_domicilios.php?id=".$aRow['id']."' class='btn btn-success btn-sm' data-toggle='modal'><center><span class='glyphicon glyphicon-edit'></span>Editar</center></a></td>";
   //		$row[] = "<td><a href='modificardocumento.php?id=".$aRow['id']."'><CENTER><span class='glyphicon glyphicon-pencil'></CENTER></span></a></td>";


	//$row[] = "<td><a href='#' data-href='eliminar.php?id=".$aRow['id']."' data-toggle='modal' data-target='#confirm-delete'><CENTER><span class='glyphicon glyphicon-trash'></span></CENTER></a></td>";
        
		$output['aaData'][] = $row;
	}
	
	echo json_encode( $output );
?>
   