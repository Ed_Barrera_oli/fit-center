<!DOCTYPE html>
<html lang="es">   <!-- Lenguaje de la pagina  -->
	<head>
	 <meta charset="UTF-8"> 
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/bootstrap-theme.css" rel="stylesheet">
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>	
	</head>
	
<body>
		<div class="container">
			<div class="row">
				<h3 style="text-align:center">NUEVO REGISTRO AIRBNB</h3>
			</div>
			
			<form class="form-horizontal" method="POST" action="guardar.php" enctype="multipart/form-data" autocomplete="off">
				<!--Nombre Completo--->
				<div class="form-group">
					<label for="NOMBRE" class="col-sm-2 control-label">Nombre Completo</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="NOMBRE" name="NOMBRE" placeholder="Nombre Completo" required>
					</div>
				</div>
                 <!--RFC--->
                 <div class="form-group">
					<label for="RFC" class="col-sm-2 control-label">RFC</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="RFC" name="RFC" placeholder="RFC" required>
					</div>
				</div>
          
                 <!--Colonia--->
                 <div class="form-group">
					<label for="COLONIA" class="col-sm-2 control-label">Colonia</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="COLONIA" name="COLONIA" placeholder="Colonia" required>
					</div>
				</div>
                 
                 <!--Municipio--->
                 <div class="form-group">
					<label for="MUNICIPIO" class="col-sm-2 control-label">Municipio</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="MUNICIPIO" name="MUNICIPIO" placeholder="Municipio" required>
					</div>
				</div>
           
                 <!--Servicio--->
                 <div class="form-group">
					<label for="SERVICIO" class="col-sm-2 control-label">Servicio</label>
					<div class="col-sm-10">
						<select class="form-control" id="SERVICIO" name="SERVICIO">
                            <option value="AIRBNB">AIRBNB</option>
						</select>
					</div>
				</div>
				
				<!--Tarjeta--->
                 <div class="form-group">
					<label for="TARJETA" class="col-sm-2 control-label">Tarjeta</label>
					<div class="col-sm-10">
						<select class="form-control" id="TARJETA" name="TARJETA">
                            <option value="BBVA">BBVA</option>
                            <option value="SANTANDER">SANTANDER</option>
                            <option value="BANAMEX">BANAMEX</option>
                            <option value="BANORTE">BANORTE</option>
                            <option value="HSBC">HSBC</option>
                            <option value="SCOTIABANCK">SCOTIABANCK</option>
                            <option value="INBURSA">INBURSA</option>
                            <option value="AFIRE">AFRIRE</option>
                            <option value="AMERICAN EXPRESS">AMERICAN EXPRESS</option>
						</select>
					</div>
				</div>
				
				<!--Digitos--->
                 <div class="form-group">
					<label for="DIGITOS" class="col-sm-2 control-label">Digitos</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="DIGITOS" name="DIGITOS" placeholder="Digitos" required>
					</div>
				</div>
				
				<!--Telefono Uno--->
                 <div class="form-group">
					<label for="TELUNO" class="col-sm-2 control-label">Telefono Uno</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="TELUNO" name="TELUNO" placeholder="Telefono Uno" required>
					</div>
				</div>
               
                <!--Celular--->
                 <div class="form-group">
					<label for="CELULAR" class="col-sm-2 control-label">Celular</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="CELULAR" name="CELULAR" placeholder="Celular" required>
					</div>
				</div>
                
				

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<a href="index.php" class="btn btn-default">Regresar</a>
						<button type="submit" class="btn btn-primary">Guardar</button>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>