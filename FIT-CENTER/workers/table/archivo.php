<?php
    require '../Sauron/table/conexion.php';
    
    $id = $_GET['id'];
    
    $sql = "SELECT * FROM documentos WHERE id = '$id'";
    $resultado = $mysqli->query($sql);
    $row = $resultado->fetch_array(MYSQLI_ASSOC);
    
?>
<html lang="es">
    <head>
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="../table/css/bootstrap.min.css" rel="stylesheet">
        <link href="../table/css/bootstrap-theme.css" rel="stylesheet">
        <script src="../table/js/jquery-3.1.1.min.js"></script>
        <script src="../table/js/bootstrap.min.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function() {
                $('.delete').click(function(){
                    var parent = $(this).parent().attr('id');
                    var service = $(this).parent().attr('data');
                    var dataString = 'id='+service;
                    
                    $.ajax({
                        type: "POST",
                        url: "del_file.php",
                        data: dataString,
                        success: function() {           
                            location.reload();
                        }
                    });
                });                 
            });    
        </script>
        
    </head>
    
    <body>
        <div class="container">
            <div class="row">
                <h3 style="text-align:center">ARCHIVO</h3>
                        <?php 
                            $path = "archivos/".$id;
                            if(file_exists($path)){
                                $directorio = opendir($path);
                                while ($archivo = readdir($directorio))
                                {

                                    if (!is_dir($archivo)){
                                        echo "<div data='".$path."/".$archivo."'><a href='".$path."/".$archivo."' title='Ver Archivo Adjunto'><CENTER><span class='glyphicon glyphicon-eye-open'></span></CENTER></a>";
                                        
                                    }
                                }
                            }
                            
                        ?>
                        
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>

