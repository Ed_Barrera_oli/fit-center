<?php
	
	
	require 'table2/conexion.php';
	
	/* Nombre de La Tabla */
	$sTabla = "saldos";
	
	/* Array que contiene los nombres de las columnas de la tabla*/
	$aColumnas = array('id',
                       'FID',
                       'SERIAL_NUM',
                       'FILE_SINCE_DT',
                       'BUREAU_ID',
                       'MEMBER_KOB',
                       'MEMBER_CODE',
                       'MEMBER_SHORT_NAME',
                       'MEMBER_AREA_CODE',
                       'MEMBER_PHONE_NUM',
                       'ACCT_NUM',
                       'ACCOUNT_STATUS',
                       'OWNER_INDIC',
                       'POSTED_DT',
                       'PREF_CUST_CODE',
                       'ACCT_TYPE',
                       'CONTRACT_TYPE',
                       'TERMS_NUM_PAYMTS',
                       'TERMS_FREQUENCY',
                       'TERMS_AMT',
                       'OPENED_DT',
                       'LAST_PAYMT_DT',
                       'LAST_PURCHASED_DT',
                       'CLOSED_DT',
                       'REPORTING_DT',
                       'REPORTING_MODE',
                       'PAID_OFF_DT',
                       'COLLATERAL',
                       'CURRENCY_CODE',
                       'HIGH_CREDIT_AMT',
                       'CUR_BALANCE_AMT',
                       'CREDIT_LIMIT',
                       'AMT_PAST_DUE',
                       'PAYMT_PAT_HST', 
                       'PAYMT_PAT_STR_DT',
                       'PAYMT_PAT_END_DT',
                       'CUR_MOP_STATUS',
                       'REMARKS_CODE',
                       'RESTRUCT_DT',
                       'SUPPRESS_SET_DT',
                       'SUPRESS_EXPIR_DT',
                       'MAX_DELINQNCY_AMT',
                       'MAX_DELINQNCY_DT',
                       'MAX_DELINQNCY_MOP',
                       'NUM_PAYMTS_LATE',
                       'NUM_MONTHS_REVIEW',
                       'NUM_PAYMTS_30_DAYS',
                       'NUM_PAYMTS_60_DAYS',
                       'NUM_PAYMTS_90_DAYS',
                       'NUM_PAYMTS_120_DAYS',
                       'APPRAISSE_VALUE',
                       'FIRST_NO_PAYMENT_DT',
                       'SALDO_INSOLUTO',
                       'LAST_PAYMT_AMT',
                       'CRC_INDIC',
                       'PLAZO_MESES',
                       'MONTO_CREDITO_ORIGINAL',
                       'LAST_PAST_DUE_DT',
                       'INTEREST_AMT',
                       'CUR_INTEREST_MOP',
                       'DAYS_PAST_DUE',
                       'EMAIL'
                      );


	
	/* columna indexada */
	$sIndexColumn = "id";
	
	// Paginacion
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".$_GET['iDisplayStart'].", ".$_GET['iDisplayLength'];
	}
	
	
	//Ordenacion
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
		{
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
			{
				$sOrder .= $aColumnas[ intval( $_GET['iSortCol_'.$i] ) ]."
				".$_GET['sSortDir_'.$i] .", ";
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	
	//Filtracion
	$sWhere = "";
	if ( $_GET['sSearch'] != "" )
	{
		$sWhere = "WHERE (";
		for ( $i=0 ; $i<count($aColumnas) ; $i++ )
		{
			$sWhere .= $aColumnas[$i]." LIKE '%".$_GET['sSearch']."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
	}
	
	// Filtrado de columna individual 
	for ( $i=0 ; $i<count($aColumnas) ; $i++ )
	{
		if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			if ( $sWhere == "" )
			{
				$sWhere = "WHERE ";
			}
			else
			{
				$sWhere .= " AND ";
			}
			$sWhere .= $aColumnas[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
		}
	}
	
	
	//Obtener datos para mostrar SQL queries
	$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumnas))."
	FROM   $sTabla
	$sWhere
	$sOrder
	$sLimit
	";
	$rResult = $mysqli->query($sQuery);
	
	/* Data set length after filtering */
	$sQuery = "
	SELECT FOUND_ROWS()
	";
	$rResultFilterTotal = $mysqli->query($sQuery);
	$aResultFilterTotal = $rResultFilterTotal->fetch_array();
	$iFilteredTotal = $aResultFilterTotal[0];
	
	/* Total data set length */
	$sQuery = "
	SELECT COUNT(".$sIndexColumn.")
	FROM   $sTabla
	";
	$rResultTotal = $mysqli->query($sQuery);
	$aResultTotal = $rResultTotal->fetch_array();
	$iTotal = $aResultTotal[0];
	
	/*
		* Output
	*/
	$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
	);
	
	while ( $aRow = $rResult->fetch_array())
	{
		$row = array();
		for ( $i=0 ; $i<count($aColumnas) ; $i++ )
		{
			if ( $aColumnas[$i] == "version" )
			{
				/* Special output formatting for 'version' column */
				$row[] = ($aRow[ $aColumnas[$i] ]=="0") ? '-' : $aRow[ $aColumnas[$i] ];
			}
			else if ( $aColumnas[$i] != ' ' )
			{
				/* General output */
				$row[] = $aRow[ $aColumnas[$i] ];
			}
		}
		//para llamar hacer funcion  class='btn btn-success btn-sm' data-toggle='modal'>
        //$row[] ="<td><a href='#edit_?id=".$aRow['id']."'><span class='glyphicon glyphicon-edit'></span> Editar</a></td>";
       	//para llamar hacer funcion   class='btn btn-danger btn-sm' data-toggle='modal''>
        //$row[] ="<td><a href='#delete_?id=".$aRow['id']."'><span class='glyphicon glyphicon-trash'></span> Eliminar</a></td>";
          
                                                    
   //  $row[] = "<td><a href='archivo.php?id=".$aRow['id']."'><CENTER><span class='glyphicon glyphicon-eye-open'></span></CENTER></a></td>";       
   	$row[] = "<td><a href='actualizar_saldos.php?id=".$aRow['id']."' class='btn btn-success btn-sm' data-toggle='modal'><center><span class='glyphicon glyphicon-edit'></span>Editar</center></a></td>";
   //		$row[] = "<td><a href='modificardocumento.php?id=".$aRow['id']."'><CENTER><span class='glyphicon glyphicon-pencil'></CENTER></span></a></td>";
	

	//$row[] = "<td><a href='#' data-href='eliminar.php?id=".$aRow['id']."' data-toggle='modal' data-target='#confirm-delete'><CENTER><span class='glyphicon glyphicon-trash'></span></CENTER></a></td>";
        
		$output['aaData'][] = $row;
	}
	
	echo json_encode( $output );
?>
   