<?php
	require '../table/conexion.php';
	
	$id = $_GET['id'];
	
	$sql = "SELECT * FROM documentos WHERE id = '$id'";
	$resultado = $mysqli->query($sql);
	$row = $resultado->fetch_array(MYSQLI_ASSOC);
	
?>
<!DOCTYPE html>
  <html lang="es">   <!-- Lenguaje de la pagina  -->
	<head>
	 <meta charset="UTF-8"> 
		<link href="../table/css/bootstrap.min.css" rel="stylesheet">
		<link href="../table/css/bootstrap-theme.css" rel="stylesheet">
		<script src="../table/js/jquery-3.1.1.min.js"></script>
		<script src="../table/js/bootstrap.min.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				$('.delete').click(function(){
					var parent = $(this).parent().attr('id');
					var service = $(this).parent().attr('data');
					var dataString = 'id='+service;
					$.ajax({
						type: "POST",
						url: "del_file.php",
						data: dataString,
						success: function() {			
							location.reload();
						}
					});
				});                 
			});    
		</script>
		
	</head>
	
	<body>
		<div class="container">
			<div class="row">
				<h3 style="text-align:center">MODIFICAR REGISTRO DE DOCUMENTO</h3>
			</div>
			
			<form class="form-horizontal" method="POST" action="updatedocumento.php" enctype="multipart/form-data" autocomplete="off">
				
	<div class="form-group">
	 <input type="hidden" id="id" name="id" value="<?php echo $row['id']; ?>" /> 
					<label for="archivo" class="col-sm-2 control-label">Archivo</label>
					<div class="col-sm-10">
						<input type="file" class="form-control" id="archivo" name="archivo"  value="<?php echo $row['nombre']; ?>" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document" required>
						
						<?php 
							$path = "archivos/".$id;
							if(file_exists($path)){
								$directorio = opendir($path);
								while ($archivo = readdir($directorio))
								{
									if (!is_dir($archivo)){
										echo "<div data='".$path."/".$archivo."'><a href='".$path."/".$archivo."' title='Ver Archivo Adjunto'><span class='glyphicon glyphicon-picture'></span></a>";
										echo "$archivo <a href='#' class='delete' title='Ver Archivo Adjunto' ><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a></div>";
                                									
										    echo "<application/pdf src='files/$id/$archivo'/>";
	
										    echo "<application/vnd.openxmlformats-officedocument.wordprocessingml.document src='files/$id/$archivo'/>";
										
									}
								}
							}
							
						?>
						
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<a href="index.php" class="btn btn-default">Regresar</a>
						
						<button type="submit" class="btn btn-primary">Guardar</button>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>
