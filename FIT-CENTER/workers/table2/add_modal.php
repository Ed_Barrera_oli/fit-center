<!-- Add New -->
<div class="modal fade" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <center>Nuevo registro</center>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" >&times;</button>
                
            </div>
            <div class="modal-body">
			<div class="container-fluid">
			<form method="POST" action="table2/add.php">
				<input type="hidden" class="form-control" name="id" value="<?php echo $row['id']; ?>">
				<div class="row form-group">
				<div class="col-sm-10">
						<label class="control-label modal-label">FID:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FID" value="<?php echo $row['FID']; ?>">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SERIAL_NUM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SERIAL_NUM" value="<?php echo $row['SERIAL_NUM']; ?>">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FILE_SINCE_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FILE_SINCE_DT" value="<?php echo $row['FILE_SINCE_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">BUREAU_ID:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="BUREAU_ID" value="<?php echo $row['BUREAU_ID']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MEMBER_KOB:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MEMBER_KOB" value="<?php echo $row['MEMBER_KOB']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MEMBER_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MEMBER_CODE" value="<?php echo $row['MEMBER_CODE']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MEMBER_SHORT_NAME:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MEMBER_SHORT_NAME" value="<?php echo $row['MEMBER_SHORT_NAME']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MEMBER_AREA_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MEMBER_AREA_CODE" value="<?php echo $row['MEMBER_AREA_CODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MEMBER_PHONE_NUM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MEMBER_PHONE_NUM" value="<?php echo $row['MEMBER_PHONE_NUM']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">ACCT_NUM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="ACCT_NUM" value="<?php echo $row['ACCT_NUM']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">ACCOUNT_STATUS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="ACCOUNT_STATUS" value="<?php echo $row['ACCOUNT_STATUS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">OWNER_INDIC:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="OWNER_INDIC" value="<?php echo $row['OWNER_INDIC']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">POSTED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="POSTED_DT" value="<?php echo $row['POSTED_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PREF_CUST_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PREF_CUST_CODE" value="<?php echo $row['PREF_CUST_CODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">ACCT_TYPE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="ACCT_TYPE" value="<?php echo $row['ACCT_TYPE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CONTRACT_TYPE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CONTRACT_TYPE" value="<?php echo $row['CONTRACT_TYPE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">TERMS_NUM_PAYMTS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="TERMS_NUM_PAYMTS" value="<?php echo $row['TERMS_NUM_PAYMTS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">TERMS_FREQUENCY:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="TERMS_FREQUENCY" value="<?php echo $row['TERMS_FREQUENCY']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">TERMS_AMT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="TERMS_AMT" value="<?php echo $row['TERMS_AMT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">OPENED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="OPENED_DT" value="<?php echo $row['OPENED_DT']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LAST_PAYMT_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LAST_PAYMT_DT" value="<?php echo $row['LAST_PAYMT_DT']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LAST_PURCHASED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LAST_PURCHASED_DT" value="<?php echo $row['LAST_PURCHASED_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CLOSED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CLOSED_DT" value="<?php echo $row['CLOSED_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">REPORTING_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="REPORTING_DT" value="<?php echo $row['REPORTING_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">REPORTING_MODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="REPORTING_MODE" value="<?php echo $row['REPORTING_MODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PAID_OFF_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PAID_OFF_DT" value="<?php echo $row['PAID_OFF_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">COLLATERAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="COLLATERAL" value="<?php echo $row['COLLATERAL']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CURRENCY_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CURRENCY_CODE" value="<?php echo $row['CURRENCY_CODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">HIGH_CREDIT_AMT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="HIGH_CREDIT_AMT" value="<?php echo $row['HIGH_CREDIT_AMT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CUR_BALANCE_AMT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CUR_BALANCE_AMT" value="<?php echo $row['CUR_BALANCE_AMT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CREDIT_LIMIT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CREDIT_LIMIT" value="<?php echo $row['CREDIT_LIMIT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">AMT_PAST_DUE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="AMT_PAST_DUE" value="<?php echo $row['AMT_PAST_DUE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PAYMT_PAT_HST:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PAYMT_PAT_HST" value="<?php echo $row['PAYMT_PAT_HST']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PAYMT_PAT_STR_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PAYMT_PAT_STR_DT" value="<?php echo $row['PAYMT_PAT_STR_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PAYMT_PAT_END_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PAYMT_PAT_END_DT" value="<?php echo $row['PAYMT_PAT_END_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CUR_MOP_STATUS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CUR_MOP_STATUS" value="<?php echo $row['CUR_MOP_STATUS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">REMARKS_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="REMARKS_CODE" value="<?php echo $row['REMARKS_CODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RESTRUCT_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RESTRUCT_DT" value="<?php echo $row['RESTRUCT_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SUPPRESS_SET_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SUPPRESS_SET_DT" value="<?php echo $row['SUPPRESS_SET_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SUPRESS_EXPIR_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SUPRESS_EXPIR_DT" value="<?php echo $row['SUPRESS_EXPIR_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MAX_DELINQNCY_AMT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MAX_DELINQNCY_AMT" value="<?php echo $row['MAX_DELINQNCY_AMT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MAX_DELINQNCY_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MAX_DELINQNCY_DT" value="<?php echo $row['MAX_DELINQNCY_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MAX_DELINQNCY_MOP:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MAX_DELINQNCY_MOP" value="<?php echo $row['MAX_DELINQNCY_MOP']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_PAYMTS_LATE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_PAYMTS_LATE" value="<?php echo $row['NUM_PAYMTS_LATE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_MONTHS_REVIEW:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_MONTHS_REVIEW" value="<?php echo $row['NUM_MONTHS_REVIEW']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_PAYMTS_30_DAYS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_PAYMTS_30_DAYS" value="<?php echo $row['NUM_PAYMTS_30_DAYS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_PAYMTS_60_DAYS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_PAYMTS_60_DAYS" value="<?php echo $row['NUM_PAYMTS_60_DAYS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_PAYMTS_90_DAYS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_PAYMTS_90_DAYS" value="<?php echo $row['NUM_PAYMTS_90_DAYS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_PAYMTS_120_DAYS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_PAYMTS_120_DAYS" value="<?php echo $row['NUM_PAYMTS_120_DAYS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">APPRAISSE_VALUE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="APPRAISSE_VALUE" value="<?php echo $row['APPRAISSE_VALUE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FIRST_NO_PAYMENT_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FIRST_NO_PAYMENT_DT" value="<?php echo $row['FIRST_NO_PAYMENT_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SALDO_INSOLUTO:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SALDO_INSOLUTO" value="<?php echo $row['SALDO_INSOLUTO']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LAST_PAYMT_AMT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LAST_PAYMT_AMT" value="<?php echo $row['LAST_PAYMT_AMT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CRC_INDIC:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CRC_INDIC" value="<?php echo $row['CRC_INDIC']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PLAZO_MESES:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PLAZO_MESES" value="<?php echo $row['PLAZO_MESES']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MONTO_CREDITO_ORIGINAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MONTO_CREDITO_ORIGINAL" value="<?php echo $row['MONTO_CREDITO_ORIGINAL']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LAST_PAST_DUE_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LAST_PAST_DUE_DT" value="<?php echo $row['LAST_PAST_DUE_DT']; ?>">
					</div>
				</div>
          <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">INTEREST_AMT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="INTEREST_AMT" value="<?php echo $row['INTEREST_AMT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CUR_INTEREST_MOP:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CUR_INTEREST_MOP" value="<?php echo $row['CUR_INTEREST_MOP']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">DAYS_PAST_DUE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="DAYS_PAST_DUE" value="<?php echo $row['DAYS_PAST_DUE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">EMAIL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="EMAIL" value="<?php echo $row['EMAIL']; ?>">
					</div>
				</div>
           
            </div> 
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                <button type="submit" name="add" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</a>
			</form>
            </div>

        </div>
    </div>
</div>