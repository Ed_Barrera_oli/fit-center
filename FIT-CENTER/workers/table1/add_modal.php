<!-- Add New -->
<div class="modal fade" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <center>Nuevo registro</center>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" >&times;</button>
                
            </div>
            <div class="modal-body">
			<div class="container-fluid">
			<form method="POST" action="table1/add.php">
				<div class="row form-group">
				<div class="col-sm-10">
						<label class="control-label modal-label">FID:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FID" value="<?php echo $row['FID']; ?>">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FILE_SINCE_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FILE_SINCE_DT" value="<?php echo $row['FILE_SINCE_DT']; ?>">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RPTED_MEMBER_KOB:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RPTED_MEMBER_KOB" value="<?php echo $row['RPTED_MEMBER_KOB']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RPTED_MEMBER:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RPTED_MEMBER" value="<?php echo $row['RPTED_MEMBER']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RPTED_RFC:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RPTED_RFC" value="<?php echo $row['RPTED_RFC']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RETED_RFC_LAST3:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RETED_RFC_LAST3" value="<?php echo $row['RETED_RFC_LAST3']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PATERNAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PATERNAL" value="<?php echo $row['PATERNAL']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MATERNAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MATERNAL" value="<?php echo $row['MATERNAL']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">ADDITIONL_SURNAME:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="ADDITIONL_SURNAME" value="<?php echo $row['ADDITIONL_SURNAME']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FIRST:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FIRST" value="<?php echo $row['FIRST']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MIDDLE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MIDDLE" value="<?php echo $row['MIDDLE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PREFIX:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PREFIX" value="<?php echo $row['PREFIX']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SUFFIX:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SUFFIX" value="<?php echo $row['SUFFIX']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MARITAL_STATUS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MARITAL_STATUS" value="<?php echo $row['MARITAL_STATUS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RESIDENT_STATUS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RESIDENT_STATUS" value="<?php echo $row['RESIDENT_STATUS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">COUNTRY_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="COUNTRY_CODE" value="<?php echo $row['COUNTRY_CODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NATLITY:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NATLITY" value="<?php echo $row['NATLITY']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SEX:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SEX" value="<?php echo $row['SEX']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">OTHER_TAX_NUM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="OTHER_TAX_NUM" value="<?php echo $row['OTHER_TAX_NUM']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">OTHER_TAX_NATLITY:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="OTHER_TAX_NATLITY" value="<?php echo $row['OTHER_TAX_NATLITY']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_DEPENDENTS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_DEPENDENTS" value="<?php echo $row['NUM_DEPENDENTS']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">BIRTH_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="BIRTH_DT" value="<?php echo $row['BIRTH_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">DECEASED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="DECEASED_DT" value="<?php echo $row['DECEASED_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">DRIVERS_LICENSE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="DRIVERS_LICENSE" value="<?php echo $row['DRIVERS_LICENSE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PROFES_LICENSE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PROFES_LICENSE" value="<?php echo $row['PROFES_LICENSE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">VOTER_REGISTR:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="VOTER_REGISTR" value="<?php echo $row['VOTER_REGISTR']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">WATCH_FLAG:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="WATCH_FLAG" value="<?php echo $row['WATCH_FLAG']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">DONT_DISPLAY:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="DONT_DISPLAY" value="<?php echo $row['DONT_DISPLAY']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NO_PROMOTE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NO_PROMOTE" value="<?php echo $row['NO_PROMOTE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MERGE_FREEZE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MERGE_FREEZE" value="<?php echo $row['MERGE_FREEZE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">OFFICER_FLAG:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="OFFICER_FLAG" value="<?php echo $row['OFFICER_FLAG']; ?>">
					</div>
				</div>
            </div> 
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                <button type="submit" name="add" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</a>
			</form>
            </div>

        </div>
    </div>
</div>