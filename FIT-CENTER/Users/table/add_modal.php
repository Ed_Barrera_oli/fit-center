<!-- Add New -->
<div class="modal fade" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <center>Nuevo registro</center>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" >&times;</button>
                
            </div>
            <div class="modal-body">
			<div class="container-fluid">
			<form method="POST" action="table/add.php">
				<div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FID:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FID" required>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LINE1:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LINE1" required>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LINE2:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LINE2" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">COLONIA:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="COLONIA" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CONICIPALITY:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CONICIPALITY" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CITY:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CITY" required>
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">STATE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="STATE" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">POSTAL_SECTION:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="POSTAL_SECTION" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">POSTAL_LAST2:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="POSTAL_LAST2" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">POSTAL_PLUS5:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="POSTAL_PLUS5" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PHONE_NUMBER:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PHONE_NUMBER" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PHONE_NUM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PHONE_NUM" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PHONE_LAST5:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PHONE_LAST5" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PHONE_EXT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PHONE_EXT" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FAX_AREA_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FAX_AREA_CODE" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FAX_PHONE_NUMBER:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FAX_PHONE_NUMBER" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FAX_PHONE_NUM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FAX_PHONE_NUM" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FAX_PHONE_LAST5:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FAX_PHONE_LAST5" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SPECIAL_INDIC:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SPECIAL_INDIC" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">USE_CNT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="USE_CNT" required>
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LAST_USED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LAST_USED_DT" required>
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RESIDENCE_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RESIDENCE_DT" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">REPT_MEMBER_KOB:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="REPT_MEMBER_KOB" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">REPT_MEMBER:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="REPT_MEMBER" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RPTED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RPTED_DT" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">TYPE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="TYPE" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SOUNDEX_PATERNAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SOUNDEX_PATERNAL" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SOUNDEX_MATERNAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SOUNDEX_MATERNAL" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SOUNDEX_ADDT_SURNM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SOUNDEX_ADDT_SURNM" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FIRST_INITIAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FIRST_INITIAL" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PATNL_PATNL_CNT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PATNL_PATNL_CNT" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PATNL_MATNL_CNT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PATNL_MATNL_CNT" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MATNL_PATNL_CNT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MATNL_PATNL_CNT" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MATNL_MATNL_CNT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MATNL_MATNL_CNT" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">COUNTRY_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="COUNTRY_CODE" required>
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">EXTRA:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="EXTRA" required>
					</div>
				</div>
            </div> 
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                <button type="submit" name="add" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</a>
			</form>
            </div>

        </div>
    </div>
</div>