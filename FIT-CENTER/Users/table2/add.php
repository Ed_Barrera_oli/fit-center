<?php
	session_start();
	include_once('../table2/connection.php');

	if(isset($_POST['add'])){
		$FID = $_POST['FID'];
		$SERIAL_NUM = $_POST['SERIAL_NUM'];
		$FILE_SINCE_DT = $_POST['FILE_SINCE_DT'];
        $BUREAU_ID = $_POST['BUREAU_ID'];
		$MEMBER_KOB = $_POST['MEMBER_KOB'];
		$MEMBER_CODE = $_POST['MEMBER_CODE'];
        $MEMBER_SHORT_NAME = $_POST['MEMBER_SHORT_NAME'];
		$MEMBER_AREA_CODE = $_POST['MEMBER_AREA_CODE'];
		$MEMBER_PHONE_NUM = $_POST['MEMBER_PHONE_NUM'];
        
        $ACCT_NUM = $_POST['ACCT_NUM'];
        $ACCOUNT_STATUS = $_POST['ACCOUNT_STATUS'];
		$OWNER_INDIC = $_POST['OWNER_INDIC'];
		$POSTED_DT = $_POST['POSTED_DT'];
        $PREF_CUST_CODE = $_POST['PREF_CUST_CODE'];
		$ACCT_TYPE = $_POST['ACCT_TYPE'];
		$CONTRACT_TYPE = $_POST['CONTRACT_TYPE'];
        $TERMS_NUM_PAYMTS = $_POST['TERMS_NUM_PAYMTS'];
        $TERMS_FREQUENCY = $_POST['TERMS_FREQUENCY'];
		$TERMS_AMT = $_POST['TERMS_AMT'];
        
		$OPENED_DT = $_POST['OPENED_DT']; 
        $LAST_PAYMT_DT = $_POST['LAST_PAYMT_DT'];
		$LAST_PURCHASED_DT = $_POST['LAST_PURCHASED_DT'];
		$CLOSED_DT = $_POST['CLOSED_DT'];
        $REPORTING_DT = $_POST['REPORTING_DT'];
		$REPORTING_MODE = $_POST['REPORTING_MODE'];
		$PAID_OFF_DT = $_POST['PAID_OFF_DT'];  
        $COLLATERAL = $_POST['COLLATERAL'];
		$CURRENCY_CODE = $_POST['CURRENCY_CODE'];
        $HIGH_CREDIT_AMT = $_POST['HIGH_CREDIT_AMT'];
        
		
		$CUR_BALANCE_AMT = $_POST['CUR_BALANCE_AMT']; 
        $CREDIT_LIMIT = $_POST['CREDIT_LIMIT'];
		$AMT_PAST_DUE = $_POST['AMT_PAST_DUE'];
		$PAYMT_PAT_HST = $_POST['PAYMT_PAT_HST'];
        $PAYMT_PAT_STR_DT = $_POST['PAYMT_PAT_STR_DT'];
		$PAYMT_PAT_END_DT = $_POST['PAYMT_PAT_END_DT'];
		$CUR_MOP_STATUS = $_POST['CUR_MOP_STATUS'];  
        $REMARKS_CODE = $_POST['REMARKS_CODE'];
		$RESTRUCT_DT = $_POST['RESTRUCT_DT'];
        $SUPPRESS_SET_DT = $_POST['SUPPRESS_SET_DT'];
        
        $SUPRESS_EXPIR_DT = $_POST['SUPRESS_EXPIR_DT'];
        $MAX_DELINQNCY_AMT = $_POST['MAX_DELINQNCY_AMT'];
		$MAX_DELINQNCY_DT = $_POST['MAX_DELINQNCY_DT'];
		$MAX_DELINQNCY_MOP = $_POST['MAX_DELINQNCY_MOP'];
        $NUM_PAYMTS_LATE = $_POST['NUM_PAYMTS_LATE'];
		$NUM_MONTHS_REVIEW = $_POST['NUM_MONTHS_REVIEW'];
		$NUM_PAYMTS_30_DAYS = $_POST['NUM_PAYMTS_30_DAYS'];  
        $NUM_PAYMTS_60_DAYS = $_POST['NUM_PAYMTS_60_DAYS'];
		$NUM_PAYMTS_90_DAYS = $_POST['NUM_PAYMTS_90_DAYS'];
        $NUM_PAYMTS_120_DAYS = $_POST['NUM_PAYMTS_120_DAYS'];
        
        $APPRAISSE_VALUE = $_POST['APPRAISSE_VALUE'];  //Aqui me quede
        $FIRST_NO_PAYMENT_DT = $_POST['FIRST_NO_PAYMENT_DT'];
		$SALDO_INSOLUTO = $_POST['SALDO_INSOLUTO'];
		$LAST_PAYMT_AMT = $_POST['LAST_PAYMT_AMT'];
        $CRC_INDIC = $_POST['CRC_INDIC'];
		$PLAZO_MESES = $_POST['PLAZO_MESES'];
		$MONTO_CREDITO_ORIGINAL = $_POST['MONTO_CREDITO_ORIGINAL'];  
        $LAST_PAST_DUE_DT = $_POST['LAST_PAST_DUE_DT'];
		$INTEREST_AMT = $_POST['INTEREST_AMT'];
        $CUR_INTEREST_MOP = $_POST['CUR_INTEREST_MOP'];
        
        $DAYS_PAST_DUE = $_POST['DAYS_PAST_DUE']; 
        $EMAIL = $_POST['EMAIL'];
		
     
      
        
		$sql = "INSERT INTO saldos (FID,
                       SERIAL_NUM,
                       FILE_SINCE_DT,
                       BUREAU_ID,
                       MEMBER_KOB,
                       MEMBER_CODE,
                       MEMBER_SHORT_NAME,
                       MEMBER_AREA_CODE,
                       MEMBER_PHONE_NUM,
                       ACCT_NUM,
                       ACCOUNT_STATUS,
                       OWNER_INDIC,
                       POSTED_DT,
                       PREF_CUST_CODE,
                       ACCT_TYPE,
                       CONTRACT_TYPE,
                       TERMS_NUM_PAYMTS,
                       TERMS_FREQUENCY,
                       TERMS_AMT,
                       OPENED_DT,
                       LAST_PAYMT_DT,
                       LAST_PURCHASED_DT,
                       CLOSED_DT,
                       REPORTING_DT,
                       REPORTING_MODE,
                       PAID_OFF_DT,
                       COLLATERAL,
                       CURRENCY_CODE,
                       HIGH_CREDIT_AMT,
                       CUR_BALANCE_AMT,
                       CREDIT_LIMIT,
                       AMT_PAST_DUE,
                       PAYMT_PAT_HST, 
                       PAYMT_PAT_STR_DT,
                       PAYMT_PAT_END_DT,
                       CUR_MOP_STATUS,
                       REMARKS_CODE,
                       RESTRUCT_DT,
                       SUPPRESS_SET_DT,
                       SUPRESS_EXPIR_DT,
                       MAX_DELINQNCY_AMT,
                       MAX_DELINQNCY_DT,
                       MAX_DELINQNCY_MOP,
                       NUM_PAYMTS_LATE,
                       NUM_MONTHS_REVIEW,
                       NUM_PAYMTS_30_DAYS,
                       NUM_PAYMTS_60_DAYS,
                       NUM_PAYMTS_90_DAYS,
                       NUM_PAYMTS_120_DAYS,
                       APPRAISSE_VALUE,
                       FIRST_NO_PAYMENT_DT,
                       SALDO_INSOLUTO,
                       LAST_PAYMT_AMT,
                       CRC_INDIC,
                       PLAZO_MESES,
                       MONTO_CREDITO_ORIGINAL,
                       LAST_PAST_DUE_DT,
                       INTEREST_AMT,
                       CUR_INTEREST_MOP,
                       DAYS_PAST_DUE,
                       EMAIL) VALUES ('$FID',
                                        '$SERIAL_NUM',
                                        '$FILE_SINCE_DT',
                                        '$BUREAU_ID',
                                        '$MEMBER_KOB',
                                        '$MEMBER_CODE',
                                        '$MEMBER_SHORT_NAME',
                                        '$MEMBER_AREA_CODE',
                                        '$MEMBER_PHONE_NUM',
                                        '$ACCT_NUM',
                                        '$ACCOUNT_STATUS',
                                        '$OWNER_INDIC',
                                        '$POSTED_DT',
                                        '$PREF_CUST_CODE',
                                        '$ACCT_TYPE',
                                        '$CONTRACT_TYPE',
                                        '$TERMS_NUM_PAYMTS',
                                        '$TERMS_FREQUENCY',
                                        '$TERMS_AMT',
                                        '$OPENED_DT',
                                        '$LAST_PAYMT_DT',
                                        '$LAST_PURCHASED_DT',
                                        '$CLOSED_DT',
                                        '$REPORTING_DT',
                                        '$REPORTING_MODE',
                                        '$PAID_OFF_DT',
                                        '$COLLATERAL',
                                        '$CURRENCY_CODE',
                                        '$HIGH_CREDIT_AMT',
                                        '$CUR_BALANCE_AMT',
                                        '$CREDIT_LIMIT',
                                        '$AMT_PAST_DUE',
                                        '$PAYMT_PAT_HST',
                                        '$PAYMT_PAT_STR_DT',
                                        '$PAYMT_PAT_END_DT',
                                        '$CUR_MOP_STATUS',
                                        '$REMARKS_CODE', 
                                        '$RESTRUCT_DT',
                                        '$SUPPRESS_SET_DT',
                                        '$SUPRESS_EXPIR_DT',
                                        '$MAX_DELINQNCY_AMT',
                                        '$MAX_DELINQNCY_DT',
                                        '$MAX_DELINQNCY_MOP',
                                        '$NUM_PAYMTS_LATE',
                                        '$NUM_MONTHS_REVIEW',
                                        '$NUM_PAYMTS_30_DAYS',
                                        '$NUM_PAYMTS_60_DAYS',
                                        '$NUM_PAYMTS_90_DAYS',
                                        '$NUM_PAYMTS_120_DAYS',
                                        '$APPRAISSE_VALUE',
                                        '$FIRST_NO_PAYMENT_DT',
                                        '$SALDO_INSOLUTO',
                                        '$LAST_PAYMT_AMT',
                                        '$CRC_INDIC',
                                        '$PLAZO_MESES',
                                        '$MONTO_CREDITO_ORIGINAL',
                                        '$LAST_PAST_DUE_DT',
                                        '$INTEREST_AMT',
                                        '$CUR_INTEREST_MOP',
                                        '$DAYS_PAST_DUE',
                                        '$EMAIL')";

		//use for MySQLi OOP
		if($conn->query($sql)){
			
            echo '<script>alert("Datos almacenados correctamente")</script> ';
		}
		
		else{
			$_SESSION['error'] = 'Algo salió mal al agregar el registro';
		}
	}
	else{
		$_SESSION['error'] = 'Rellena el formulario de agregar primero';
	}

	header('location: ../saldos.php');
?>