
<!DOCTYPE html>

<?php
session_start();
if (@!$_SESSION['user']) {
	header("Location:../index.php");
    }
?>
<?php
	require 'table1/conexion.php';
	
	$where = "";
	
	if(!empty($_POST))
	{
		$valor = $_POST['campo'];  //valor de campo                                                  
		if(!empty($valor)){         //si esta vacio es por que no se encuentra 
			$where = "WHERE nombre LIKE '%$valor'";   //like% es para buscar con pocas palabras de nombre 
		}
	}
	$sql = "SELECT * FROM nombres $where LIMIT 100";//declara la variable
	$resultado = $mysqli->query($sql);
	
?>

<html lang="en">

<head>
    <title>MULTI DATABASE</title>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta name="keywords" content="bootstrap, bootstrap admin template, admin theme, admin dashboard, dashboard template, admin template, responsive" />
    <meta name="author" content="luisreyes" />
    <!-- Favicon icon -->
    <link rel="icon" href="assets/images/simbolo-biohazard.png" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <!-- waves.css -->
    <link rel="stylesheet" href="assets/pages/waves/css/waves.min.css" type="text/css" media="all">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap/css/bootstrap.min.css">
    <!-- waves.css -->
    <link rel="stylesheet" href="assets/pages/waves/css/waves.min.css" type="text/css" media="all">
    <!-- themify icon -->
    <link rel="stylesheet" type="text/css" href="assets/icon/themify-icons/themify-icons.css">
    <!-- font-awesome-n -->
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome-n.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <!-- scrollbar.css -->
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.mCustomScrollbar.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="table1/datatable/dataTable.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="table1/bootstrap/css/bootstrap.min.css">
     


    
    <script src="assets/js/jquery-3.1.1.min.js"></script>
 <script>
    let temp = $("#btn1").clone();
$("#btn1").click(function(){
    $("#btn1").after(temp);
});
$(document).ready(function(){
	//inialize datatable
    //$('#myTable').DataTable();
     var table = $('#myTable').DataTable({
         orderCellsTop: true,
         fixedHeader: true,
          "lengthMenu": [ [100, 250, 500, 1000], [100, 250, 500, 1000] ],
         
         "language":{
             
					"lengthMenu": "Mostrar _MENU_ registros por pagina",
					"info": "Mostrando pagina _PAGE_ de _PAGES_",
						"infoEmpty": "No hay registros disponibles",
						"infoFiltered": "(filtrada de _MAX_ registros)",
						"loadingRecords": "Cargando...",
						"processing":     "Procesando...",
                          
						"search": "Buscar:",
						"zeroRecords":    "No se encontraron registros coincidentes",
						"paginate": {
							"next":       "Siguiente",
							"previous":   "Anterior"
						},					
					},
         
         "bProcessing": true,
					"bServerSide": true,
					"sAjaxSource": "server_process_nombre.php"	
     });
    
       //Creamos una fila en el head de la tabla y lo clonamos para cada columna
    $('#myTable thead tr').clone(true).appendTo( '#myTable thead' );

    $('#myTable thead tr:eq(1) th').each( function (i) {
        var title = $(this).text(); //es el nombre de la columna
        $(this).html( '<input type="text" placeholder="Search...'+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
   
   
   
});
    //hide alert
   // $(document).on('click', '.close', function(){
    //	$('.alert').hide();
    //})
//});
</script>
                    
       
					
			
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">
                        <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                            <i class="ti-menu"></i>
                        </a>
                        <div class="mobile-search waves-effect waves-light">
                            <div class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-prepend search-close"><i class="ti-close input-group-text"></i></span>
                                        <input type="text" class="form-control" placeholder="Enter Keyword">
                                        <span class="input-group-append search-btn"><i class="ti-search input-group-text"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="MultidbUser.php">
                            <img class="img-fluid" src="assets/images/logo.png" alt="Theme-Logo" />
                        </a>
                        <a class="mobile-options waves-effect waves-light">
                            <i class="ti-more"></i>
                        </a>
                    </div>
                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li>
                                <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                                    <i class="ti-fullscreen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="header-notification">
                                <a href="#!" class="waves-effect waves-light">
                                    <i class="ti-bell"></i>
                                    <span class="badge bg-c-red"></span>
                                </a>
                               
                            </li>
                            <li class="user-profile header-notification">
                                <a href="#!" class="waves-effect waves-light">
                                    <img src="assets/images/avatar.png" class="img-radius" alt="User-Profile-Image">
                                    <span><strong><?php echo $_SESSION['user'];?></strong></span>
                                    <i class="ti-angle-down"></i>
                                </a>
                                <ul class="show-notification profile-notification">
                                    <li class="waves-effect waves-light">
                                        <a href="../desconectar.php">
                                            <i class="ti-layout-sidebar-left"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="">
                                <div class="main-menu-header">
                                    <img class="img-80 img-radius" src="assets/images/avatar.png" alt="User-Profile-Image">
                                    <div class="user-details">
                                        <span id="more-details"><strong><?php echo $_SESSION['user'];?></strong><i class="fa fa-caret-down"></i></span>
                                    </div>
                                </div>
                                <div class="main-menu-content">
                                    <ul>
                                        <li class="more-details">
                                            
                                            <a href="../desconectar.php"><i class="ti-layout-sidebar-left"></i>Logout</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pcoded-navigation-label">DataBase</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i><b>BC</b></span>
                                        <span class="pcoded-mtext">DataBase</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="domicilios.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Domicilios</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="nombre.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Nombres</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="saldos.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Saldos</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        
                                    </ul>
                                </li>
                            </ul>
                             <div class="pcoded-navigation-label">Maps</div>
                            <ul class="pcoded-item pcoded-left-item">                         
                                <li class="">
                                    <a href="map-google.php" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-map-alt"></i><b>M</b></span>
                                        <span class="pcoded-mtext">Maps</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                            
                        </div>
                    </nav>
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">
                                                SAURON
                                            </h5>
                                            <p class="m-b-0">Welcome to SAURON</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="MultidbUser.php"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">MULTIDATABSE</a>    
                    
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">DATABASE</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-body start -->
                                    <div class="page-body">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Nombres</h5>
                                                <span>DB Nombres</span>
                                                <div class="card-header-right">
                                                    <ul class="list-unstyled card-option">
                                                        <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                        
                                                        <li><i class="fa fa-minus minimize-card"></i></li>
                                                        <li><i class="fa fa-refresh reload-card"></i></li>
                                                        <li><i class="fa fa-trash close-card"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                           <div class="card-block table-border-style">
                                     			 <div class="table-responsive">
				<a href="#addnew" data-toggle="modal" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
				
			</div>
			
			<div class="table-responsive">
				<table id="myTable" class="table table-bordered table-striped">
					<thead>
					<tr>
					<!--1-->	<th>ID</th>
					<!--2-->	<th>FID</th>
					<!--3-->	<th>FILE_SINCE_DT</th>
					<!--4-->	<th>RPTED_MEMBER_KOB</th>
					<!--5-->	<th>RPTED_MEMBER</th>    
					<!--6-->	<th>RPTED_RFC</th>
					<!--7-->	<th>RETED_RFC_LAST3</th>
					<!--8-->	<th>PATERNAL</th>
					<!--9-->	<th>MATERNAL</th>
					<!--10-->	<th>ADDITIONL_SURNAME</th>
					<!--11-->	<th>FIRST</th>
					<!--12-->	<th>MIDDLE</th>
					<!--13-->	<th>PREFIX</th>    
					<!--14-->	<th>SUFFIX</th>
					<!--15-->	<th>MARITAL_STATUS</th>
					<!--16-->	<th>RESIDENT_STATUS</th>
					<!--17-->	<th>COUNTRY_CODE</th>
					<!--18-->	<th>NATLITY</th>
					<!--19-->	<th>SEX</th>
					<!--20-->	<th>OTHER_TAX_NUM</th>
					<!--21-->	<th>OTHER_TAX_NATLITY</th>    
					<!--22-->	<th>NUM_DEPENDENTS</th>
					<!--23-->	<th>BIRTH_DT</th>
					<!--24-->	<th>DECEASED_DT</th>
					<!--25-->	<th>DRIVERS_LICENSE</th>
					<!--26-->	<th>PROFES_LICENSE</th>
					<!--27-->	<th>VOTER_REGISTR</th>
					<!--28-->	<th>WATCH_FLAG</th>
					<!--29-->	<th>DONT_DISPLAY</th>    
					<!--30-->	<th>NO_PROMOTE</th>
					<!--31-->	<th>MERGE_FREEZE</th>
					<!--32-->	<th>OFFICER_FLAG</th>
					
					<th>EDITAR</th>
					
						
						</tr>
					</thead>
					
					<tbody>
						
                                    <?php while($row = $resultado->fetch_array(MYSQLI_ASSOC)) { ?>
								<tr>
							<!--1-->		<td><?php echo $row['id']; ?></td>
							<!--2-->		<td><?php echo $row['FID']; ?></td>
							<!--3-->		<td><?php echo $row['FILE_SINCE_DT']; ?></td>
							<!--4-->		<td><?php echo $row['RPTED_MEMBER_KOB']; ?></td>
							<!--5-->		<td><?php echo $row['RPTED_MEMBER']; ?></td>
							<!--6-->		<td><?php echo $row['RPTED_RFC']; ?></td>
							<!--7-->		<td><?php echo $row['RETED_RFC_LAST3']; ?></td>
							<!--8-->		<td><?php echo $row['PATERNAL']; ?></td>
                            <!--9-->        <td><?php echo $row['MATERNAL']; ?></td>
				            <!--10-->		<td><?php echo $row['ADDITIONL_SURNAME']; ?></td>
				            <!--11-->		<td><?php echo $row['FIRST']; ?></td>
				            <!--12-->     	<td><?php echo $row['MIDDLE']; ?></td>
							<!--13-->		<td><?php echo $row['PREFIX']; ?></td>
							<!--14-->		<td><?php echo $row['SUFFIX']; ?></td>
							<!--15-->		<td><?php echo $row['MARITAL_STATUS']; ?></td>
							<!--16-->		<td><?php echo $row['RESIDENT_STATUS']; ?></td>
                            <!--17-->        <td><?php echo $row['COUNTRY_CODE']; ?></td>
							<!--18-->		<td><?php echo $row['NATLITY']; ?></td>
							<!--19-->		<td><?php echo $row['SEX']; ?></td>
							<!--20-->		<td><?php echo $row['OTHER_TAX_NUM']; ?></td>
							<!--21-->		<td><?php echo $row['OTHER_TAX_NATLITY']; ?></td>
							<!--22-->		<td><?php echo $row['NUM_DEPENDENTS']; ?></td>
							<!--23-->		<td><?php echo $row['BIRTH_DT']; ?></td>
							<!--24-->		<td><?php echo $row['DECEASED_DT']; ?></td>
                            <!--25-->        <td><?php echo $row['DRIVERS_LICENSE']; ?></td>
							<!--26-->		<td><?php echo $row['PROFES_LICENSE']; ?></td>
							<!--27-->		<td><?php echo $row['VOTER_REGISTR']; ?></td>
							<!--28-->		<td><?php echo $row['WATCH_FLAG']; ?></td>
							<!--29-->		<td><?php echo $row['DONT_DISPLAY']; ?></td>
							<!--30-->		<td><?php echo $row['NO_PROMOTE']; ?></td>
							<!--31-->		<td><?php echo $row['MERGE_FREEZE']; ?></td>
							<!--32-->		<td><?php echo $row['OFFICER_FLAG']; ?></td>
                           
                      
                           
                          
                         

								</tr>
					
						<?php } ?>
					</tbody>
						
				</table>
                                                
                                                </div>
                                            </div>
                                             </div>
                                        
                                    </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                            </div>
                            <!-- Main-body end -->

                            <div id="styleSelector">

                            </div>
                        </div> 
                    </div>
                </div>
            </div>

		
 
			   
 <?php include('table1/add_modal.php') ?>
<!-- Modal ocupado para la visualizacion del boton de eliminar -->
		<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					
					<div class="modal-header">
					<P>Eliminar Registro</P>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						
					</div>
					
					<div class="modal-body">
						¿Desea eliminar este registro?
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<a class="btn btn-danger btn-ok">Delete</a>
					</div>
				</div>
			</div>
		</div>
		
			
    <!-- Required Jquery -->
    <script type="text/javascript" src="assets/js/jquery/jquery.min.js "></script>
    <script type="text/javascript" src="assets/js/jquery-ui/jquery-ui.min.js "></script>
    <script type="text/javascript" src="assets/js/popper.js/popper.min.js"></script>
   
    <!-- waves js -->
    <script src="assets/pages/waves/js/waves.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="assets/js/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- Custom js -->
    <script src="assets/js/pcoded.min.js"></script>
    <script src="assets/js/vertical/vertical-layout.min.js"></script>
    <script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
	
   
<script src="table1/bootstrap/js/bootstrap.min.js"></script>
<script src="table1/datatable/jquery.dataTables.min.js"></script>
<script src="table1/datatable/dataTable.bootstrap.min.js"></script>
<script>
			$('#confirm-delete').on('show.bs.modal', function(e) {
				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
				
				$('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
			});
		</script>







<!-- generate datatable on our table -->

   
   
    
</body>

</html>
