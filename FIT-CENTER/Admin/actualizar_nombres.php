	
<!DOCTYPE html>

<?php
session_start();
if (@!$_SESSION['user']) {
	header("Location:../index.php");
    }
?>
<?php
	require 'table1/conexion.php';
	
	$id = $_GET['id'];
	
	$sql = "SELECT * FROM nombres WHERE id = '$id'";
	$resultado = $mysqli->query($sql);
	$row = $resultado->fetch_array(MYSQLI_ASSOC);
	
?>

<html lang="en">

<head>
    <title>MULTI DATABASE</title>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta name="keywords" content="bootstrap, bootstrap admin template, admin theme, admin dashboard, dashboard template, admin template, responsive" />
    <meta name="author" content="Codedthemes" />
    <!-- Favicon icon -->
    <link rel="icon" href="assets/images/simbolo-biohazard.png" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <!-- waves.css -->
    <link rel="stylesheet" href="assets/pages/waves/css/waves.min.css" type="text/css" media="all">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap/css/bootstrap.min.css">
    <!-- waves.css -->
    <link rel="stylesheet" href="assets/pages/waves/css/waves.min.css" type="text/css" media="all">
    <!-- themify icon -->
    <link rel="stylesheet" type="text/css" href="assets/icon/themify-icons/themify-icons.css">
    <!-- font-awesome-n -->
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome-n.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <!-- scrollbar.css -->
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.mCustomScrollbar.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">
                        <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                            <i class="ti-menu"></i>
                        </a>
                        <div class="mobile-search waves-effect waves-light">
                            <div class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-prepend search-close"><i class="ti-close input-group-text"></i></span>
                                        <input type="text" class="form-control" placeholder="Enter Keyword">
                                        <span class="input-group-append search-btn"><i class="ti-search input-group-text"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="Multidb.php">
                            <img class="img-fluid" src="assets/images/logo.png" alt="Theme-Logo" />
                        </a>
                        <a class="mobile-options waves-effect waves-light">
                            <i class="ti-more"></i>
                        </a>
                    </div>
                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li>
                                <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                                    <i class="ti-fullscreen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="header-notification">
                                <a href="#!" class="waves-effect waves-light">
                                    <i class="ti-bell"></i>
                                    <span class="badge bg-c-red"></span>
                                </a>
                               
                            </li>
                            <li class="user-profile header-notification">
                                <a href="#!" class="waves-effect waves-light">
                                    <img src="assets/images/avatar.png" class="img-radius" alt="User-Profile-Image">
                                    <span><strong><?php echo $_SESSION['user'];?></strong></span>
                                    <i class="ti-angle-down"></i>
                                </a>
                                <ul class="show-notification profile-notification">
                                    <li class="waves-effect waves-light">
                                        <a href="../desconectar.php">
                                            <i class="ti-layout-sidebar-left"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="">
                                <div class="main-menu-header">
                                    <img class="img-80 img-radius" src="assets/images/avatar.png" alt="User-Profile-Image">
                                    <div class="user-details">
                                        <span id="more-details"><strong><?php echo $_SESSION['user'];?></strong><i class="fa fa-caret-down"></i></span>
                                    </div>
                                </div>
                                <div class="main-menu-content">
                                    <ul>
                                        <li class="more-details">
                                            <a href="users.php"><i class="ti-user"></i>View Profile</a>
                                            <a href="../desconectar.php"><i class="ti-layout-sidebar-left"></i>Logout</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pcoded-navigation-label">DataBase</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i><b>BC</b></span>
                                        <span class="pcoded-mtext">DataBase</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="domicilios.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Domicilios</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="nombre.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Nombres</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="saldos.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Saldos</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        
                                    </ul>
                                </li>
                            </ul>
                             <div class="pcoded-navigation-label">Maps</div>
                            <ul class="pcoded-item pcoded-left-item">                         
                                <li class="">
                                    <a href="map-google.php" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-map-alt"></i><b>M</b></span>
                                        <span class="pcoded-mtext">Maps</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                            <div class="pcoded-navigation-label">Pages</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu ">
                                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-id-badge"></i><b>A</b></span>
                                        <span class="pcoded-mtext">Pages</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="">
                                            <a href="users.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Users</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                         <li class="">
                                            <a href="registro.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Registration</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">UPDATE USERS SAURON</h5>
                                            <p class="m-b-0">UPDATE USER</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="Multidb.php"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">MULTIDATABASE</a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">UPDATE DOMICILIOS</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">

                                    <!-- Page body start -->
                                    <div class="page-body">
                                        
                                            
                                                   
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <!-- Basic Form Inputs card start -->
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Nombres</h5>
                                                        <span>UPDATE NOMBRES</span>
                                                    </div>
                                                   <center> <h2>UPDATE NOMBRES</h2>	</center>
		
		
		<div class="card-block">
		
	
		<form method="POST" action="table1/edit.php">
				<input type="hidden" class="form-control" name="id" value="<?php echo $row['id']; ?>">
				<div class="row form-group">
				<div class="col-sm-10">
						<label class="control-label modal-label">FID:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FID" value="<?php echo $row['FID']; ?>">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FILE_SINCE_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FILE_SINCE_DT" value="<?php echo $row['FILE_SINCE_DT']; ?>">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RPTED_MEMBER_KOB:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RPTED_MEMBER_KOB" value="<?php echo $row['RPTED_MEMBER_KOB']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RPTED_MEMBER:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RPTED_MEMBER" value="<?php echo $row['RPTED_MEMBER']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RPTED_RFC:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RPTED_RFC" value="<?php echo $row['RPTED_RFC']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RETED_RFC_LAST3:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RETED_RFC_LAST3" value="<?php echo $row['RETED_RFC_LAST3']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PATERNAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PATERNAL" value="<?php echo $row['PATERNAL']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MATERNAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MATERNAL" value="<?php echo $row['MATERNAL']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">ADDITIONL_SURNAME:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="ADDITIONL_SURNAME" value="<?php echo $row['ADDITIONL_SURNAME']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FIRST:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FIRST" value="<?php echo $row['FIRST']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MIDDLE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MIDDLE" value="<?php echo $row['MIDDLE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PREFIX:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PREFIX" value="<?php echo $row['PREFIX']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SUFFIX:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SUFFIX" value="<?php echo $row['SUFFIX']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MARITAL_STATUS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MARITAL_STATUS" value="<?php echo $row['MARITAL_STATUS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RESIDENT_STATUS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RESIDENT_STATUS" value="<?php echo $row['RESIDENT_STATUS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">COUNTRY_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="COUNTRY_CODE" value="<?php echo $row['COUNTRY_CODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NATLITY:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NATLITY" value="<?php echo $row['NATLITY']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SEX:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SEX" value="<?php echo $row['SEX']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">OTHER_TAX_NUM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="OTHER_TAX_NUM" value="<?php echo $row['OTHER_TAX_NUM']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">OTHER_TAX_NATLITY:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="OTHER_TAX_NATLITY" value="<?php echo $row['OTHER_TAX_NATLITY']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_DEPENDENTS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_DEPENDENTS" value="<?php echo $row['NUM_DEPENDENTS']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">BIRTH_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="BIRTH_DT" value="<?php echo $row['BIRTH_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">DECEASED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="DECEASED_DT" value="<?php echo $row['DECEASED_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">DRIVERS_LICENSE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="DRIVERS_LICENSE" value="<?php echo $row['DRIVERS_LICENSE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PROFES_LICENSE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PROFES_LICENSE" value="<?php echo $row['PROFES_LICENSE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">VOTER_REGISTR:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="VOTER_REGISTR" value="<?php echo $row['VOTER_REGISTR']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">WATCH_FLAG:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="WATCH_FLAG" value="<?php echo $row['WATCH_FLAG']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">DONT_DISPLAY:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="DONT_DISPLAY" value="<?php echo $row['DONT_DISPLAY']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NO_PROMOTE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NO_PROMOTE" value="<?php echo $row['NO_PROMOTE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MERGE_FREEZE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MERGE_FREEZE" value="<?php echo $row['MERGE_FREEZE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">OFFICER_FLAG:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="OFFICER_FLAG" value="<?php echo $row['OFFICER_FLAG']; ?>">
					</div>
				</div>
            </div> 
			</div>
            <div class="modal-footer">
               	
						
                <button onclick="history.back(-1);" type="button" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                <button type="submit" name="edit" class="btn btn-success"><span class="glyphicon glyphicon-check"></span> Actualizar</a>
			</form>
                                               
                                        </div>
                                    </div>
                                    <!-- Page body end -->
                                </div>
                            </div>
                            <!-- Main-body end -->
                            <div id="styleSelector">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


   
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="assets/js/jquery/jquery.min.js "></script>
    <script type="text/javascript" src="assets/js/jquery-ui/jquery-ui.min.js "></script>
    <script type="text/javascript" src="assets/js/popper.js/popper.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/js/bootstrap.min.js "></script>
    <!-- waves js -->
    <script src="assets/pages/waves/js/waves.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="assets/js/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Custom js -->
    <script src="assets/js/pcoded.min.js"></script>
    <script src="assets/js/vertical/vertical-layout.min.js"></script>
    <script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
    
</body>

</html>







