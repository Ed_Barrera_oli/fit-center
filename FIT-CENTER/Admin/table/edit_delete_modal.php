 	
<!-- Edit -->
<div class="modal fade" id="edit_<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel">Editar Datos</h4></center>
            </div>
            <div class="modal-body">
			<div class="container-fluid">
			<form method="POST" action="../Admin/table/edit.php">
				<input type="hidden" class="form-control" name="id" value="<?php echo $row['id']; ?>">
				<div class="row form-group">
				<div class="col-sm-10">
						<label class="control-label modal-label">FID:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FID" value="<?php echo $row['FID']; ?>">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LINE1:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LINE1" value="<?php echo $row['LINE1']; ?>">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LINE2:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LINE2" value="<?php echo $row['LINE2']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">COLONIA:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="COLONIA" value="<?php echo $row['COLONIA']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CONICIPALITY:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CONICIPALITY" value="<?php echo $row['CONICIPALITY']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CITY:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CITY" value="<?php echo $row['CITY']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">STATE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="STATE" value="<?php echo $row['STATE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">POSTAL_SECTION:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="POSTAL_SECTION" value="<?php echo $row['POSTAL_SECTION']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">POSTAL_LAST2:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="POSTAL_LAST2" value="<?php echo $row['POSTAL_LAST2']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">POSTAL_PLUS5:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="POSTAL_PLUS5" value="<?php echo $row['POSTAL_PLUS5']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PHONE_NUMBER:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PHONE_NUMBER" value="<?php echo $row['PHONE_NUMBER']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PHONE_NUM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PHONE_NUM" value="<?php echo $row['PHONE_NUM']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PHONE_LAST5:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PHONE_LAST5" value="<?php echo $row['PHONE_LAST5']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PHONE_EXT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PHONE_EXT" value="<?php echo $row['PHONE_EXT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FAX_AREA_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FAX_AREA_CODE" value="<?php echo $row['FAX_AREA_CODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FAX_PHONE_NUMBER:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FAX_PHONE_NUMBER" value="<?php echo $row['FAX_PHONE_NUMBER']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FAX_PHONE_NUM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FAX_PHONE_NUM" value="<?php echo $row['FAX_PHONE_NUM']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FAX_PHONE_LAST5:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FAX_PHONE_LAST5" value="<?php echo $row['FAX_PHONE_LAST5']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SPECIAL_INDIC:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SPECIAL_INDIC" value="<?php echo $row['SPECIAL_INDIC']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">USE_CNT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="USE_CNT" value="<?php echo $row['USE_CNT']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LAST_USED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LAST_USED_DT" value="<?php echo $row['LAST_USED_DT']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RESIDENCE_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RESIDENCE_DT" value="<?php echo $row['RESIDENCE_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">REPT_MEMBER_KOB:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="REPT_MEMBER_KOB" value="<?php echo $row['REPT_MEMBER_KOB']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">REPT_MEMBER:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="REPT_MEMBER" value="<?php echo $row['REPT_MEMBER']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RPTED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RPTED_DT" value="<?php echo $row['RPTED_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">TYPE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="TYPE" value="<?php echo $row['TYPE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SOUNDEX_PATERNAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SOUNDEX_PATERNAL" value="<?php echo $row['SOUNDEX_PATERNAL']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SOUNDEX_MATERNAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SOUNDEX_MATERNAL" value="<?php echo $row['SOUNDEX_MATERNAL']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SOUNDEX_ADDT_SURNM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SOUNDEX_ADDT_SURNM" value="<?php echo $row['SOUNDEX_ADDT_SURNM']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FIRST_INITIAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FIRST_INITIAL" value="<?php echo $row['FIRST_INITIAL']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PATNL_PATNL_CNT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PATNL_PATNL_CNT" value="<?php echo $row['PATNL_PATNL_CNT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PATNL_MATNL_CNT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PATNL_MATNL_CNT" value="<?php echo $row['PATNL_MATNL_CNT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MATNL_PATNL_CNT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MATNL_PATNL_CNT" value="<?php echo $row['MATNL_PATNL_CNT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MATNL_MATNL_CNT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MATNL_MATNL_CNT" value="<?php echo $row['MATNL_MATNL_CNT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">COUNTRY_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="COUNTRY_CODE" value="<?php echo $row['COUNTRY_CODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">EXTRA:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="EXTRA" value="<?php echo $row['EXTRA']; ?>">
					</div>
				</div>
            </div> 
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                <button type="submit" name="edit" class="btn btn-success"><span class="glyphicon glyphicon-check"></span> Actualizar</a>
			</form>
            </div>

        </div>
    </div>
</div>
<!-- Delete -->
<div class="modal fade" id="delete_<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel">Eliminar Miembro</h4></center>
            </div>
            <div class="modal-body">	
            	<p class="text-center">Estas seguro que quieres borrarlo?</p>
				<h2 class="text-center"><?php echo $row['FID']; ?></h2>
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                <a href="../Admin/table/delete.php?id=<?php echo $row['id']; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Si, estoy seguro</a>
            </div>

        </div>
    </div>
</div>
