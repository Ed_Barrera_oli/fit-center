<?php
	session_start();
	include_once('../table/connection.php');

	if(isset($_POST['add'])){
		$FID = $_POST['FID'];
		$LINE1 = $_POST['LINE1'];
		$LINE2 = $_POST['LINE2'];
        $COLONIA = $_POST['COLONIA'];
		$CONICIPALITY = $_POST['CONICIPALITY'];
		$CITY = $_POST['CITY'];
        $STATE = $_POST['STATE'];
		$POSTAL_SECTION = $_POST['POSTAL_SECTION'];
		$POSTAL_LAST2 = $_POST['POSTAL_LAST2'];
        
        $POSTAL_PLUS5 = $_POST['POSTAL_PLUS5'];
        $PHONE_NUMBER = $_POST['PHONE_NUMBER'];
		$PHONE_NUM = $_POST['PHONE_NUM'];
		$PHONE_LAST5 = $_POST['PHONE_LAST5'];
        $PHONE_EXT = $_POST['PHONE_EXT'];
		$FAX_AREA_CODE = $_POST['FAX_AREA_CODE'];
		$FAX_PHONE_NUMBER = $_POST['FAX_PHONE_NUMBER'];
        $FAX_PHONE_NUM = $_POST['FAX_PHONE_NUM'];
        $FAX_PHONE_LAST5 = $_POST['FAX_PHONE_LAST5'];
		$SPECIAL_INDIC = $_POST['SPECIAL_INDIC'];
        
		$USE_CNT = $_POST['USE_CNT'];
        $LAST_USED_DT = $_POST['LAST_USED_DT'];
		$RESIDENCE_DT = $_POST['RESIDENCE_DT'];
		$REPT_MEMBER_KOB = $_POST['REPT_MEMBER_KOB'];
        $REPT_MEMBER = $_POST['REPT_MEMBER'];
		$RPTED_DT = $_POST['RPTED_DT'];
		$TYPE = $_POST['TYPE'];  
        $SOUNDEX_PATERNAL = $_POST['SOUNDEX_PATERNAL'];
		$SOUNDEX_MATERNAL = $_POST['SOUNDEX_MATERNAL'];
        $SOUNDEX_ADDT_SURNM = $_POST['SOUNDEX_ADDT_SURNM'];
        
		$FIRST_INITIAL = $_POST['FIRST_INITIAL'];
		$PATNL_PATNL_CNT = $_POST['PATNL_PATNL_CNT'];
        $PATNL_MATNL_CNT = $_POST['PATNL_MATNL_CNT'];
		$MATNL_PATNL_CNT = $_POST['MATNL_PATNL_CNT'];
		$MATNL_MATNL_CNT = $_POST['MATNL_MATNL_CNT'];
        $COUNTRY_CODE = $_POST['COUNTRY_CODE'];
		$EXTRA = $_POST['EXTRA'];
		
     
      
        
		$sql = "INSERT INTO domicilios (FID,
                                        LINE1, 
                                        LINE2, 
                                        COLONIA, 
                                        CONICIPALITY,
                                        CITY, 
                                        STATE, 
                                        POSTAL_SECTION, 
                                        POSTAL_LAST2, 
                                        POSTAL_PLUS5, 
                                        PHONE_NUMBER, 
                                        PHONE_NUM, 
                                        PHONE_LAST5, 
                                        PHONE_EXT, 
                                        FAX_AREA_CODE, 
                                        FAX_PHONE_NUMBER, 
                                        FAX_PHONE_NUM, 
                                        FAX_PHONE_LAST5, 
                                        SPECIAL_INDIC, 
                                        USE_CNT, 
                                        LAST_USED_DT, 
                                        RESIDENCE_DT, 
                                        REPT_MEMBER_KOB, 
                                        REPT_MEMBER, 
                                        RPTED_DT, 
                                        TYPE, 
                                        SOUNDEX_PATERNAL, 
                                        SOUNDEX_MATERNAL, 
                                        SOUNDEX_ADDT_SURNM, 
                                        FIRST_INITIAL, 
                                        PATNL_PATNL_CNT, 
                                        PATNL_MATNL_CNT, 
                                        MATNL_PATNL_CNT, 
                                        MATNL_MATNL_CNT, 
                                        COUNTRY_CODE, 
                                        EXTRA) VALUES ('$FID',
                                        '$LINE1',
                                        '$LINE2',
                                        '$COLONIA',
                                        '$CONICIPALITY',
                                        '$CITY',
                                        '$STATE',
                                        '$POSTAL_SECTION',
                                        '$POSTAL_LAST2',
                                        '$POSTAL_PLUS5',
                                        '$PHONE_NUMBER',
                                        '$PHONE_NUM',
                                        '$PHONE_LAST5',
                                        '$PHONE_EXT',
                                        '$FAX_AREA_CODE',
                                        '$FAX_PHONE_NUMBER',
                                        '$FAX_PHONE_NUM',
                                        '$FAX_PHONE_LAST5',
                                        '$SPECIAL_INDIC',
                                        '$USE_CNT',
                                        '$LAST_USED_DT',
                                        '$RESIDENCE_DT',
                                        '$REPT_MEMBER_KOB',
                                        '$REPT_MEMBER',
                                        '$RPTED_DT',
                                        '$TYPE',
                                        '$SOUNDEX_PATERNAL',
                                        '$SOUNDEX_MATERNAL',
                                        '$SOUNDEX_ADDT_SURNM',
                                        '$FIRST_INITIAL',
                                        '$PATNL_PATNL_CNT',
                                        '$PATNL_MATNL_CNT',
                                        '$MATNL_PATNL_CNT',
                                        '$MATNL_MATNL_CNT',
                                        '$COUNTRY_CODE', '$EXTRA')";

		//use for MySQLi OOP
		if($conn->query($sql)){
			
            echo '<script>alert("Datos almacenados correctamente")</script> ';
		}
		
		else{
			$_SESSION['error'] = 'Algo salió mal al agregar el registro';
		}
	}
	else{
		$_SESSION['error'] = 'Rellena el formulario de agregar primero';
	}

	header('location: ../domicilios.php');
?>