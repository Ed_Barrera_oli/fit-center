
<!DOCTYPE html>

<?php
session_start();
if (@!$_SESSION['user']) {
	header("Location:../index.php");
    }
?>
<?php
	require 'table/conexion.php';
	
	$where = "";
	
	if(!empty($_POST))
	{
		$valor = $_POST['campo'];  //valor de campo                                                  
		if(!empty($valor)){         //si esta vacio es por que no se encuentra 
			$where = "WHERE nombre LIKE '%$valor'";   //like% es para buscar con pocas palabras de nombre 
		}
	}
	$sql = "SELECT * FROM domicilios $where LIMIT 100";//declara la variable
	$resultado = $mysqli->query($sql);
	
?>

<html lang="en">

<head>
    <title>MULTI DATABASE</title>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta name="keywords" content="bootstrap, bootstrap admin template, admin theme, admin dashboard, dashboard template, admin template, responsive" />
    <meta name="author" content="luisreyes" />
    <!-- Favicon icon -->
    <link rel="icon" href="assets/images/simbolo-biohazard.png" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <!-- waves.css -->
    <link rel="stylesheet" href="assets/pages/waves/css/waves.min.css" type="text/css" media="all">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap/css/bootstrap.min.css">
    <!-- waves.css -->
    <link rel="stylesheet" href="assets/pages/waves/css/waves.min.css" type="text/css" media="all">
    <!-- themify icon -->
    <link rel="stylesheet" type="text/css" href="assets/icon/themify-icons/themify-icons.css">
    <!-- font-awesome-n -->
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome-n.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <!-- scrollbar.css -->
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.mCustomScrollbar.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="table/datatable/dataTable.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="table/bootstrap/css/bootstrap.min.css">
     


    
    <script src="assets/js/jquery-3.1.1.min.js"></script>
 <script>
    let temp = $("#btn1").clone();
$("#btn1").click(function(){
    $("#btn1").after(temp);
});
$(document).ready(function(){
	//inialize datatable
    //$('#myTable').DataTable();
     var table = $('#myTable').DataTable({
         orderCellsTop: true,
         fixedHeader: true,
          "lengthMenu": [ [100, 250, 500, 1000], [100, 250, 500, 1000] ],
         
         "language":{
             
					"lengthMenu": "Mostrar _MENU_ registros por pagina",
					"info": "Mostrando pagina _PAGE_ de _PAGES_",
						"infoEmpty": "No hay registros disponibles",
						"infoFiltered": "(filtrada de _MAX_ registros)",
						"loadingRecords": "Cargando...",
						"processing":     "Procesando...",
                          
						"search": "Buscar:",
						"zeroRecords":    "No se encontraron registros coincidentes",
						"paginate": {
							"next":       "Siguiente",
							"previous":   "Anterior"
						},					
					},
         
         "bProcessing": true,
					"bServerSide": true,
					"sAjaxSource": "server_process.php"	
     });
    
       //Creamos una fila en el head de la tabla y lo clonamos para cada columna
    $('#myTable thead tr').clone(true).appendTo( '#myTable thead' );

    $('#myTable thead tr:eq(1) th').each( function (i) {
        var title = $(this).text(); //es el nombre de la columna
        $(this).html( '<input type="text" placeholder="Search...'+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
   
   
   
});
    //hide alert
   // $(document).on('click', '.close', function(){
    //	$('.alert').hide();
    //})
//});
</script>
                    
       
					
			
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">
                        <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                            <i class="ti-menu"></i>
                        </a>
                        <div class="mobile-search waves-effect waves-light">
                            <div class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-prepend search-close"><i class="ti-close input-group-text"></i></span>
                                        <input type="text" class="form-control" placeholder="Enter Keyword">
                                        <span class="input-group-append search-btn"><i class="ti-search input-group-text"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="Multidb.php">
                            <img class="img-fluid" src="assets/images/logo.png" alt="Theme-Logo" />
                        </a>
                        <a class="mobile-options waves-effect waves-light">
                            <i class="ti-more"></i>
                        </a>
                    </div>
                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li>
                                <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                                    <i class="ti-fullscreen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="header-notification">
                                <a href="#!" class="waves-effect waves-light">
                                    <i class="ti-bell"></i>
                                    <span class="badge bg-c-red"></span>
                                </a>
                               
                            </li>
                            <li class="user-profile header-notification">
                                <a href="#!" class="waves-effect waves-light">
                                    <img src="assets/images/avatar.png" class="img-radius" alt="User-Profile-Image">
                                    <span><strong><?php echo $_SESSION['user'];?></strong></span>
                                    <i class="ti-angle-down"></i>
                                </a>
                                <ul class="show-notification profile-notification">
                                    <li class="waves-effect waves-light">
                                        <a href="../desconectar.php">
                                            <i class="ti-layout-sidebar-left"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="">
                                <div class="main-menu-header">
                                    <img class="img-80 img-radius" src="assets/images/avatar.png" alt="User-Profile-Image">
                                    <div class="user-details">
                                        <span id="more-details"><strong><?php echo $_SESSION['user'];?></strong><i class="fa fa-caret-down"></i></span>
                                    </div>
                                </div>
                                <div class="main-menu-content">
                                    <ul>
                                        <li class="more-details">
                                            <a href="users.php"><i class="ti-user"></i>View Profile</a>
                                            <a href="../desconectar.php"><i class="ti-layout-sidebar-left"></i>Logout</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pcoded-navigation-label">DataBase</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i><b>BC</b></span>
                                        <span class="pcoded-mtext">DataBase</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="domicilios.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Domicilios</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="nombre.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Nombres</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="saldos.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Saldos</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        
                                    </ul>
                                </li>
                            </ul>
                             <div class="pcoded-navigation-label">Maps</div>
                            <ul class="pcoded-item pcoded-left-item">                         
                                <li class="">
                                    <a href="map-google.php" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-map-alt"></i><b>M</b></span>
                                        <span class="pcoded-mtext">Maps</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                            <div class="pcoded-navigation-label">Pages</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu ">
                                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-id-badge"></i><b>A</b></span>
                                        <span class="pcoded-mtext">Pages</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="">
                                            <a href="users.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Users</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                         <li class="">
                                            <a href="registro.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Registration</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">
                                                SAURON
                                            </h5>
                                            <p class="m-b-0">Welcome to SAURON</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="Multidb.php"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">MULTIDATABSE</a>    
                    
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">DATABASE</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-body start -->
                                    <div class="page-body">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Domicilios</h5>
                                                <span>DB Domicilios</span>
                                                <div class="card-header-right">
                                                    <ul class="list-unstyled card-option">
                                                        <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                        
                                                        <li><i class="fa fa-minus minimize-card"></i></li>
                                                        <li><i class="fa fa-refresh reload-card"></i></li>
                                                        <li><i class="fa fa-trash close-card"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                           <div class="card-block table-border-style">
                                     			 <div class="table-responsive">
				<a href="#addnew" data-toggle="modal" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
				
			</div>
			
			<div class="table-responsive">
				<table id="myTable" class="table table-bordered table-striped">
					<thead>
					<tr>
					<!--1-->	<th>ID</th>
					<!--2-->	<th>FID</th>
					<!--3-->	<th>LINE1</th>
					<!--4-->	<th>LINE2</th>
					<!--5-->	<th>COLONIA</th>    
					<!--6-->	<th>CONICIPALITY</th>
					<!--7-->	<th>CITY</th>
					<!--8-->	<th>STATE</th>
					<!--9-->	<th>POSTAL_SECTION</th>
					<!--10-->	<th>POSTAL_LAST2</th>
					<!--11-->	<th>POSTAL_PLUS5</th>
					<!--12-->	<th>PHONE_NUMBER</th>
					<!--13-->	<th>PHONE_NUM</th>    
					<!--14-->	<th>PHONE_LAST5</th>
					<!--15-->	<th>PHONE_EXT</th>
					<!--16-->	<th>FAX_AREA_CODE</th>
					<!--17-->	<th>FAX_PHONE_NUMBER</th>
					<!--18-->	<th>FAX_PHONE_NUM</th>
					<!--19-->	<th>FAX_PHONE_LAST5</th>
					<!--20-->	<th>SPECIAL_INDIC</th>
					<!--21-->	<th>USE_CNT</th>    
					<!--22-->	<th>LAST_USED_DT</th>
					<!--23-->	<th>RESIDENCE_DT</th>
					<!--24-->	<th>REPT_MEMBER_KOB</th>
					<!--25-->	<th>REPT_MEMBER</th>
					<!--26-->	<th>RPTED_DT</th>
					<!--27-->	<th>TYPE</th>
					<!--28-->	<th>SOUNDEX_PATERNAL</th>
					<!--29-->	<th>SOUNDEX_MATERNAL</th>    
					<!--30-->	<th>SOUNDEX_ADDT_SURNM</th>
					<!--31-->	<th>FIRST_INITIAL</th>
					<!--32-->	<th>PATNL_PATNL_CNT</th>
					<!--33-->	<th>PATNL_MATNL_CNT</th>
				    <!--34-->    <th>MATNL_PATNL_CNT</th>
					<!--35-->	<th>MATNL_MATNL_CNT</th>
					<!--36-->	<th>COUNTRY_CODE</th>
					<!--37-->	<th>EXTRA</th>
					<th>EDITAR</th>
					<th>ELIMINAR</th>
						
						</tr>
					</thead>
					
					<tbody>
						
                                    <?php while($row = $resultado->fetch_array(MYSQLI_ASSOC)) { ?>
								<tr>
							<!--1-->		<td><?php echo $row['id']; ?></td>
							<!--2-->		<td><?php echo $row['FID']; ?></td>
							<!--3-->		<td><?php echo $row['LINE1']; ?></td>
							<!--4-->		<td><?php echo $row['LINE2']; ?></td>
							<!--5-->		<td><?php echo $row['COLONIA']; ?></td>
							<!--6-->		<td><?php echo $row['CONICIPALITY']; ?></td>
							<!--7-->		<td><?php echo $row['CITY']; ?></td>
							<!--8-->		<td><?php echo $row['STATE']; ?></td>
                            <!--9-->        <td><?php echo $row['POSTAL_SECTION']; ?></td>
				            <!--10-->		<td><?php echo $row['POSTAL_LAST2']; ?></td>
				            <!--11-->		<td><?php echo $row['POSTAL_PLUS5']; ?></td>
				            <!--12-->     	<td><?php echo $row['PHONE_NUMBER']; ?></td>
							<!--13-->		<td><?php echo $row['PHONE_NUM']; ?></td>
							<!--14-->		<td><?php echo $row['PHONE_LAST5']; ?></td>
							<!--15-->		<td><?php echo $row['PHONE_EXT']; ?></td>
							<!--16-->		<td><?php echo $row['FAX_AREA_CODE']; ?></td>
                            <!--17-->        <td><?php echo $row['FAX_PHONE_NUMBER']; ?></td>
							<!--18-->		<td><?php echo $row['FAX_PHONE_NUM']; ?></td>
							<!--19-->		<td><?php echo $row['FAX_PHONE_LAST5']; ?></td>
							<!--20-->		<td><?php echo $row['SPECIAL_INDIC']; ?></td>
							<!--21-->		<td><?php echo $row['USE_CNT']; ?></td>
							<!--22-->		<td><?php echo $row['LAST_USED_DT']; ?></td>
							<!--23-->		<td><?php echo $row['RESIDENCE_DT']; ?></td>
							<!--24-->		<td><?php echo $row['REPT_MEMBER_KOB']; ?></td>
                            <!--25-->        <td><?php echo $row['REPT_MEMBER']; ?></td>
							<!--26-->		<td><?php echo $row['RPTED_DT']; ?></td>
							<!--27-->		<td><?php echo $row['TYPE']; ?></td>
							<!--28-->		<td><?php echo $row['SOUNDEX_PATERNAL']; ?></td>
							<!--29-->		<td><?php echo $row['SOUNDEX_MATERNAL']; ?></td>
							<!--30-->		<td><?php echo $row['SOUNDEX_ADDT_SURNM']; ?></td>
							<!--31-->		<td><?php echo $row['FIRST_INITIAL']; ?></td>
							<!--32-->		<td><?php echo $row['PATNL_PATNL_CNT']; ?></td>
                            <!--33-->        <td><?php echo $row['PATNL_MATNL_CNT']; ?></td>
							<!--34-->		<td><?php echo $row['MATNL_PATNL_CNT']; ?></td>
							<!--35-->		<td><?php echo $row['MATNL_MATNL_CNT']; ?></td>
							<!--36-->		<td><?php echo $row['COUNTRY_CODE']; ?></td>
							<!--37-->		<td><?php echo $row['EXTRA']; ?></td>
                      
                           
                          
                         

								</tr>
					
						<?php } ?>
					</tbody>
						
				</table>
                                                
                                                </div>
                                            </div>
                                             </div>
                                        
                                    </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                            </div>
                            <!-- Main-body end -->

                            <div id="styleSelector">

                            </div>
                        </div> 
                    </div>
                </div>
            </div>

		
 
			   
 <?php include('table/add_modal.php') ?>
<!-- Modal ocupado para la visualizacion del boton de eliminar -->
		<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					
					<div class="modal-header">
					<P>Eliminar Registro</P>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						
					</div>
					
					<div class="modal-body">
						¿Desea eliminar este registro?
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<a class="btn btn-danger btn-ok">Delete</a>
					</div>
				</div>
			</div>
		</div>
		
			
    <!-- Required Jquery -->
    <script type="text/javascript" src="assets/js/jquery/jquery.min.js "></script>
    <script type="text/javascript" src="assets/js/jquery-ui/jquery-ui.min.js "></script>
    <script type="text/javascript" src="assets/js/popper.js/popper.min.js"></script>
   
    <!-- waves js -->
    <script src="assets/pages/waves/js/waves.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="assets/js/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- Custom js -->
    <script src="assets/js/pcoded.min.js"></script>
    <script src="assets/js/vertical/vertical-layout.min.js"></script>
    <script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
	
   
<script src="table/bootstrap/js/bootstrap.min.js"></script>
<script src="table/datatable/jquery.dataTables.min.js"></script>
<script src="table/datatable/dataTable.bootstrap.min.js"></script>
<script>
			$('#confirm-delete').on('show.bs.modal', function(e) {
				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
				
				$('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
			});
		</script>







<!-- generate datatable on our table -->

   
   
    
</body>

</html>
