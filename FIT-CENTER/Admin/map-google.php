
<!DOCTYPE html>

<?php
session_start();
if (@!$_SESSION['user']) {
	header("Location:../index.php");
    }
?>
<!-- data base  --->
<?php
	require '../Admin/table/conexion.php';
	
	$where = "";
	
	if(!empty($_POST))
	{
		$valor = $_POST['campo'];  //valor de campo                                                  
		if(!empty($valor)){         //si esta vacio es por que no se encuentra 
			$where = "WHERE nombre LIKE '%$valor'";   //like% es para buscar con pocas palabras de nombre 
		}
	}
	$sql = "SELECT * FROM gestion $where";//declara la variable
	$resultado = $mysqli->query($sql);
	
?>
<html lang="en">

<head>
    <title>MULTI DATABASE</title>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta name="keywords" content="bootstrap, bootstrap admin template, admin theme, admin dashboard, dashboard template, admin template, responsive" />
    <meta name="author" content="Codedthemes" />
    <!-- Favicon icon -->
    <link rel="icon" href="assets/images/simbolo-biohazard.png" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <!-- waves.css -->
    <link rel="stylesheet" href="assets/pages/waves/css/waves.min.css" type="text/css" media="all">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap/css/bootstrap.min.css">
    <!-- waves.css -->
    <link rel="stylesheet" href="assets/pages/waves/css/waves.min.css" type="text/css" media="all">
    <!-- themify icon -->
    <link rel="stylesheet" type="text/css" href="assets/icon/themify-icons/themify-icons.css">
    <!-- font-awesome-n -->
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome-n.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <!-- scrollbar.css -->
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.mCustomScrollbar.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <meta name="csrf-token" id="csrf-token" content="iX1yXtY4PAeH3ro1NOxFytAukbbi3Cs0awrZUBJY">
    <link media="all" type="text/css" rel="stylesheet" href="https://maps-website.com/css/bootstrap.min.css">
     <link media="all" type="text/css" rel="stylesheet" href="https://maps-website.com/css/custom.css">
    <link media="all" type="text/css" rel="stylesheet" href="https://maps-website.com/css/jquery-ui.min.css">
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">
                        <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                            <i class="ti-menu"></i>
                        </a>
                        <div class="mobile-search waves-effect waves-light">
                            <div class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-prepend search-close"><i class="ti-close input-group-text"></i></span>
                                        <input type="text" class="form-control" placeholder="Enter Keyword">
                                        <span class="input-group-append search-btn"><i class="ti-search input-group-text"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="Multidb.php">
                            <img class="img-fluid" src="assets/images/logo.png" alt="Theme-Logo" />
                        </a>
                        <a class="mobile-options waves-effect waves-light">
                            <i class="ti-more"></i>
                        </a>
                    </div>
                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li>
                                <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                                    <i class="ti-fullscreen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="header-notification">
                                <a href="#!" class="waves-effect waves-light">
                                    <i class="ti-bell"></i>
                                    <span class="badge bg-c-red"></span>
                                </a>
                               
                            </li>
                            <li class="user-profile header-notification">
                                <a href="#!" class="waves-effect waves-light">
                                    <img src="assets/images/avatar.png" class="img-radius" alt="User-Profile-Image">
                                    <span><strong><?php echo $_SESSION['user'];?></strong></span>
                                    <i class="ti-angle-down"></i>
                                </a>
                                <ul class="show-notification profile-notification">
                                    <li class="waves-effect waves-light">
                                        <a href="../desconectar.php">
                                            <i class="ti-layout-sidebar-left"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="">
                                <div class="main-menu-header">
                                    <img class="img-80 img-radius" src="assets/images/avatar.png" alt="User-Profile-Image">
                                    <div class="user-details">
                                        <span id="more-details"><strong><?php echo $_SESSION['user'];?></strong><i class="fa fa-caret-down"></i></span>
                                    </div>
                                </div>
                                <div class="main-menu-content">
                                    <ul>
                                        <li class="more-details">
                                            <a href="users.php"><i class="ti-user"></i>View Profile</a>
                                            <a href="../desconectar.php"><i class="ti-layout-sidebar-left"></i>Logout</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pcoded-navigation-label">DataBase</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i><b>BC</b></span>
                                        <span class="pcoded-mtext">DataBase</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="domicilios.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Domicilios</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="nombre.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Nombres</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="saldos.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Saldos</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        
                                    </ul>
                                </li>
                            </ul>
                             <div class="pcoded-navigation-label">Maps</div>
                            <ul class="pcoded-item pcoded-left-item">                         
                                <li class="">
                                    <a href="map-google.php" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-map-alt"></i><b>M</b></span>
                                        <span class="pcoded-mtext">Maps</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                            <div class="pcoded-navigation-label">Pages</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu ">
                                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-id-badge"></i><b>A</b></span>
                                        <span class="pcoded-mtext">Pages</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="">
                                            <a href="users.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Users</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                         <li class="">
                                            <a href="registro.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Registration</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">
                                                SAURON
                                            </h5>
                                            <p class="m-b-0">Welcome to SAURON</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="Multidb.php"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">MULTIDATABASE</a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">SAURON MAPS</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page body start -->
                                    <div class="page-body">
                                        <div class="row">
                                           
                                        
                                           
                                          <div class="col-lg-12 col-xl-6">
                                                <!-- Geo-Coding map start -->
                                              <div class="card">
                                                    <div class="card-header">
                                                        <h5>Geo-SAURON</h5>
                                                        <span>SEARCH YOUR LOCATION</span>
                                                    </div>
                                                    <div class="card-block">
                                                        
                                       
                                      



<div class="container">


<div class="col-md-5">
<div class="custom-container">
<div class="custom-container-title">SEARCH</div>
<input type="hidden" id="geoip_country" value="Mexico">
<input type="hidden" id="geoip_lat" value="18.8312">
<input type="hidden" id="geoip_lon" value="-98.9571">

<label for="street">Calle</label>
<input type="text" id="street" name="street" class="form-control" placeholder="Calle">
<br />
<div class="row">
<div class="col-md-6">
<label for="zip_code">Código Postal</label>
<input type="text" id="zip" name="zip_code" class="form-control" placeholder="Opcional" value="62740">
</div>
<div class="col-md-6">
<label for="city">Ciudad</label>
<input type="text" id="city" name="city" class="form-control" placeholder="Ciudad" value="Cuautla">
</div>
</div>
<hr>
<div class="row">
<div class="col-md-3">
<label for="map_type">Tipo de Mapa</label>
</div>
<div class="col-md-9">
<select id="map_type_combo" class="form-control">
<option value="roadmap" selected> Mapa de Carreteras </option>
<option value="satellite"> Satélite </option>
<option value="hybrid"> Satélite con nombre de las calles </option>
<option value="terrain"> Terreno </option>
</select>
</div>
</div>
<br>
<div class="row">
<div class="col-md-3">
<label for="zoom_combo">Zoom</label>
</div>
<div class="col-md-9">
<select id="zoom_combo" class="form-control">
<option value="21"> 2.5 m</option>
<option value="20"> 5 m</option>
<option value="19"> 10 m</option>
<option value="18"> 20 m</option>
<option value="17"> 50 m</option>
<option value="16"> 100 m</option>
<option value="15"> 200 m</option>
<option value="14"> 400 m</option>
<option value="13"> 1 km</option>
<option value="12" selected> 2 km</option>
<option value="11"> 4 km</option>
<option value="10"> 8 km</option>
<option value="9"> 15 km</option>
<option value="8"> 30 km</option>
<option value="7"> 50 km</option>
<option value="6"> 100 km</option>
<option value="5"> 200 km</option>
<option value="4"> 400 km</option>
<option value="3"> 1000 km</option>
<option value="2"> 2000 km</option>
</select>
</div>
</div>
<hr>
<div class="row">
<div class="col-md-7">
<label for="map_height_slider">Altura del Mapa</label>
<div id="map_height_slider"></div>
</div>
<div class="col-md-5">
<div class="input-group">
<input id="map_height_value" type="number" class="form-control" value="400">
<span class="input-group-addon">px</span>
</div>
</div>
</div>
<br>
<div class="row">
<div class="col-md-7">
<label for="map_width_slider">Anchura del Mapa</label>
<div id="map_width_slider"></div>
</div>
<div class="col-md-5">
<div class="input-group">
<input id="map_width_value" type="number" class="form-control" value="400">
<span class="input-group-addon">px</span>

</div>
<CENTER><input  class="btn btn-danger" type="submit" name="submit" value="SEARCH" onclick=""/></CENTER>
	                                                         
</div>
</div>
</div>
                                    
                                        
                                       
                                    
                                            <!-- Marker Clustering map end -->
                                        </div>
                                  
                                <!-- Page body end -->
                                
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-6">
                        <div class="row">
                                             
<div id="map-container" class="custom-container" style="width:545px; height:435px;">
<div class="custom-container-title">Google-Maps Vista Previa</div>
<iframe id="map-canvas" width="400" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=62740+Cuautla&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe>
</div>
                <!-- Main-body end -->

                <div id="styleSelector">

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>



<!-- Warning Section Starts -->
<!-- Older IE warning message -->
    <!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->
<!-- Required Jquery -->
<script type="text/javascript" src="assets/js/jquery/jquery.min.js "></script>
<script type="text/javascript" src="assets/js/jquery-ui/jquery-ui.min.js "></script>
<script type="text/javascript" src="assets/js/popper.js/popper.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap/js/bootstrap.min.js "></script>
<!-- waves js -->
<script src="assets/pages/waves/js/waves.min.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="assets/js/jquery-slimscroll/jquery.slimscroll.js"></script>
<!-- Google map js -->
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="assets/pages/google-maps/gmaps.js"></script>
<!-- Custom js -->
<script type="text/javascript" src="assets/pages/google-maps/google-maps.js"></script>
<script src="assets/js/pcoded.min.js"></script>
<script src="assets/js/vertical/vertical-layout.min.js"></script>
<script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="assets/js/script.js"></script>
<script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1032289389;
        var google_conversion_label = "d0xzCK_k5gMQ7fid7AM";
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
<noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt=""
                 src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1032289389/?value=1.00&amp;label=d0xzCK_k5gMQ7fid7AM&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
    
<script src="https://maps-website.com/js/jquery-ui.min.js"></script>
<script src="https://maps-website.com/js/bootstrap.min.js"></script>
<script src="https://maps-website.com/js/clipboard.min.js"></script>
<script src="https://maps-website.com/js/custom.js?v=2"></script>

</body>

</html>
