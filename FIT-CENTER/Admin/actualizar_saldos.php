	
<!DOCTYPE html>

<?php
session_start();
if (@!$_SESSION['user']) {
	header("Location:../index.php");
    }
?>
<?php
	require 'table1/conexion.php';
	
	$id = $_GET['id'];
	
	$sql = "SELECT * FROM saldos WHERE id = '$id'";
	$resultado = $mysqli->query($sql);
	$row = $resultado->fetch_array(MYSQLI_ASSOC);
	
?>

<html lang="en">

<head>
    <title>MULTI DATABASE</title>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta name="keywords" content="bootstrap, bootstrap admin template, admin theme, admin dashboard, dashboard template, admin template, responsive" />
    <meta name="author" content="Codedthemes" />
    <!-- Favicon icon -->
    <link rel="icon" href="assets/images/simbolo-biohazard.png" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <!-- waves.css -->
    <link rel="stylesheet" href="assets/pages/waves/css/waves.min.css" type="text/css" media="all">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap/css/bootstrap.min.css">
    <!-- waves.css -->
    <link rel="stylesheet" href="assets/pages/waves/css/waves.min.css" type="text/css" media="all">
    <!-- themify icon -->
    <link rel="stylesheet" type="text/css" href="assets/icon/themify-icons/themify-icons.css">
    <!-- font-awesome-n -->
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome-n.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <!-- scrollbar.css -->
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.mCustomScrollbar.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">
                        <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                            <i class="ti-menu"></i>
                        </a>
                        <div class="mobile-search waves-effect waves-light">
                            <div class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-prepend search-close"><i class="ti-close input-group-text"></i></span>
                                        <input type="text" class="form-control" placeholder="Enter Keyword">
                                        <span class="input-group-append search-btn"><i class="ti-search input-group-text"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="Multidb.php">
                            <img class="img-fluid" src="assets/images/logo.png" alt="Theme-Logo" />
                        </a>
                        <a class="mobile-options waves-effect waves-light">
                            <i class="ti-more"></i>
                        </a>
                    </div>
                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li>
                                <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                                    <i class="ti-fullscreen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="header-notification">
                                <a href="#!" class="waves-effect waves-light">
                                    <i class="ti-bell"></i>
                                    <span class="badge bg-c-red"></span>
                                </a>
                               
                            </li>
                            <li class="user-profile header-notification">
                                <a href="#!" class="waves-effect waves-light">
                                    <img src="assets/images/avatar.png" class="img-radius" alt="User-Profile-Image">
                                    <span><strong><?php echo $_SESSION['user'];?></strong></span>
                                    <i class="ti-angle-down"></i>
                                </a>
                                <ul class="show-notification profile-notification">
                                    <li class="waves-effect waves-light">
                                        <a href="../desconectar.php">
                                            <i class="ti-layout-sidebar-left"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="">
                                <div class="main-menu-header">
                                    <img class="img-80 img-radius" src="assets/images/avatar.png" alt="User-Profile-Image">
                                    <div class="user-details">
                                        <span id="more-details"><strong><?php echo $_SESSION['user'];?></strong><i class="fa fa-caret-down"></i></span>
                                    </div>
                                </div>
                                <div class="main-menu-content">
                                    <ul>
                                        <li class="more-details">
                                            <a href="users.php"><i class="ti-user"></i>View Profile</a>
                                            <a href="../desconectar.php"><i class="ti-layout-sidebar-left"></i>Logout</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pcoded-navigation-label">DataBase</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i><b>BC</b></span>
                                        <span class="pcoded-mtext">DataBase</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="domicilios.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Domicilios</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="nombre.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Nombres</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="saldos.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Saldos</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        
                                    </ul>
                                </li>
                            </ul>
                             <div class="pcoded-navigation-label">Maps</div>
                            <ul class="pcoded-item pcoded-left-item">                         
                                <li class="">
                                    <a href="map-google.php" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-map-alt"></i><b>M</b></span>
                                        <span class="pcoded-mtext">Maps</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                            <div class="pcoded-navigation-label">Pages</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu ">
                                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-id-badge"></i><b>A</b></span>
                                        <span class="pcoded-mtext">Pages</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="">
                                            <a href="users.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Users</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                         <li class="">
                                            <a href="registro.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Registration</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">UPDATE USERS SAURON</h5>
                                            <p class="m-b-0">UPDATE USER</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="Multidb.php"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">MULTIDATABASE</a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">UPDATE DOMICILIOS</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">

                                    <!-- Page body start -->
                                    <div class="page-body">
                                        
                                            
                                                   
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <!-- Basic Form Inputs card start -->
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Nombres</h5>
                                                        <span>UPDATE SALDOS</span>
                                                    </div>
                                                   <center> <h2>UPDATE SALDOS</h2>	</center>
		
		
		<div class="card-block">
		
	
		<form method="POST" action="table2/edit.php">
				<input type="hidden" class="form-control" name="id" value="<?php echo $row['id']; ?>">
				<div class="row form-group">
				<div class="col-sm-10">
						<label class="control-label modal-label">FID:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FID" value="<?php echo $row['FID']; ?>">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SERIAL_NUM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SERIAL_NUM" value="<?php echo $row['SERIAL_NUM']; ?>">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FILE_SINCE_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FILE_SINCE_DT" value="<?php echo $row['FILE_SINCE_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">BUREAU_ID:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="BUREAU_ID" value="<?php echo $row['BUREAU_ID']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MEMBER_KOB:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MEMBER_KOB" value="<?php echo $row['MEMBER_KOB']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MEMBER_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MEMBER_CODE" value="<?php echo $row['MEMBER_CODE']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MEMBER_SHORT_NAME:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MEMBER_SHORT_NAME" value="<?php echo $row['MEMBER_SHORT_NAME']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MEMBER_AREA_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MEMBER_AREA_CODE" value="<?php echo $row['MEMBER_AREA_CODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MEMBER_PHONE_NUM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MEMBER_PHONE_NUM" value="<?php echo $row['MEMBER_PHONE_NUM']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">ACCT_NUM:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="ACCT_NUM" value="<?php echo $row['ACCT_NUM']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">ACCOUNT_STATUS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="ACCOUNT_STATUS" value="<?php echo $row['ACCOUNT_STATUS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">OWNER_INDIC:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="OWNER_INDIC" value="<?php echo $row['OWNER_INDIC']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">POSTED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="POSTED_DT" value="<?php echo $row['POSTED_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PREF_CUST_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PREF_CUST_CODE" value="<?php echo $row['PREF_CUST_CODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">ACCT_TYPE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="ACCT_TYPE" value="<?php echo $row['ACCT_TYPE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CONTRACT_TYPE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CONTRACT_TYPE" value="<?php echo $row['CONTRACT_TYPE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">TERMS_NUM_PAYMTS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="TERMS_NUM_PAYMTS" value="<?php echo $row['TERMS_NUM_PAYMTS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">TERMS_FREQUENCY:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="TERMS_FREQUENCY" value="<?php echo $row['TERMS_FREQUENCY']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">TERMS_AMT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="TERMS_AMT" value="<?php echo $row['TERMS_AMT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">OPENED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="OPENED_DT" value="<?php echo $row['OPENED_DT']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LAST_PAYMT_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LAST_PAYMT_DT" value="<?php echo $row['LAST_PAYMT_DT']; ?>">
					</div>
				</div>
            <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LAST_PURCHASED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LAST_PURCHASED_DT" value="<?php echo $row['LAST_PURCHASED_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CLOSED_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CLOSED_DT" value="<?php echo $row['CLOSED_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">REPORTING_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="REPORTING_DT" value="<?php echo $row['REPORTING_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">REPORTING_MODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="REPORTING_MODE" value="<?php echo $row['REPORTING_MODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PAID_OFF_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PAID_OFF_DT" value="<?php echo $row['PAID_OFF_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">COLLATERAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="COLLATERAL" value="<?php echo $row['COLLATERAL']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CURRENCY_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CURRENCY_CODE" value="<?php echo $row['CURRENCY_CODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">HIGH_CREDIT_AMT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="HIGH_CREDIT_AMT" value="<?php echo $row['HIGH_CREDIT_AMT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CUR_BALANCE_AMT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CUR_BALANCE_AMT" value="<?php echo $row['CUR_BALANCE_AMT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CREDIT_LIMIT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CREDIT_LIMIT" value="<?php echo $row['CREDIT_LIMIT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">AMT_PAST_DUE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="AMT_PAST_DUE" value="<?php echo $row['AMT_PAST_DUE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PAYMT_PAT_HST:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PAYMT_PAT_HST" value="<?php echo $row['PAYMT_PAT_HST']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PAYMT_PAT_STR_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PAYMT_PAT_STR_DT" value="<?php echo $row['PAYMT_PAT_STR_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PAYMT_PAT_END_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PAYMT_PAT_END_DT" value="<?php echo $row['PAYMT_PAT_END_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CUR_MOP_STATUS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CUR_MOP_STATUS" value="<?php echo $row['CUR_MOP_STATUS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">REMARKS_CODE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="REMARKS_CODE" value="<?php echo $row['REMARKS_CODE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">RESTRUCT_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="RESTRUCT_DT" value="<?php echo $row['RESTRUCT_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SUPPRESS_SET_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SUPPRESS_SET_DT" value="<?php echo $row['SUPPRESS_SET_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SUPRESS_EXPIR_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SUPRESS_EXPIR_DT" value="<?php echo $row['SUPRESS_EXPIR_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MAX_DELINQNCY_AMT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MAX_DELINQNCY_AMT" value="<?php echo $row['MAX_DELINQNCY_AMT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MAX_DELINQNCY_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MAX_DELINQNCY_DT" value="<?php echo $row['MAX_DELINQNCY_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MAX_DELINQNCY_MOP:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MAX_DELINQNCY_MOP" value="<?php echo $row['MAX_DELINQNCY_MOP']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_PAYMTS_LATE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_PAYMTS_LATE" value="<?php echo $row['NUM_PAYMTS_LATE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_MONTHS_REVIEW:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_MONTHS_REVIEW" value="<?php echo $row['NUM_MONTHS_REVIEW']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_PAYMTS_30_DAYS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_PAYMTS_30_DAYS" value="<?php echo $row['NUM_PAYMTS_30_DAYS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_PAYMTS_60_DAYS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_PAYMTS_60_DAYS" value="<?php echo $row['NUM_PAYMTS_60_DAYS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_PAYMTS_90_DAYS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_PAYMTS_90_DAYS" value="<?php echo $row['NUM_PAYMTS_90_DAYS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">NUM_PAYMTS_120_DAYS:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="NUM_PAYMTS_120_DAYS" value="<?php echo $row['NUM_PAYMTS_120_DAYS']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">APPRAISSE_VALUE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="APPRAISSE_VALUE" value="<?php echo $row['APPRAISSE_VALUE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">FIRST_NO_PAYMENT_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="FIRST_NO_PAYMENT_DT" value="<?php echo $row['FIRST_NO_PAYMENT_DT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">SALDO_INSOLUTO:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="SALDO_INSOLUTO" value="<?php echo $row['SALDO_INSOLUTO']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LAST_PAYMT_AMT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LAST_PAYMT_AMT" value="<?php echo $row['LAST_PAYMT_AMT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CRC_INDIC:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CRC_INDIC" value="<?php echo $row['CRC_INDIC']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">PLAZO_MESES:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="PLAZO_MESES" value="<?php echo $row['PLAZO_MESES']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">MONTO_CREDITO_ORIGINAL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="MONTO_CREDITO_ORIGINAL" value="<?php echo $row['MONTO_CREDITO_ORIGINAL']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">LAST_PAST_DUE_DT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="LAST_PAST_DUE_DT" value="<?php echo $row['LAST_PAST_DUE_DT']; ?>">
					</div>
				</div>
          <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">INTEREST_AMT:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="INTEREST_AMT" value="<?php echo $row['INTEREST_AMT']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">CUR_INTEREST_MOP:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="CUR_INTEREST_MOP" value="<?php echo $row['CUR_INTEREST_MOP']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">DAYS_PAST_DUE:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="DAYS_PAST_DUE" value="<?php echo $row['DAYS_PAST_DUE']; ?>">
					</div>
				</div>
           <div class="row form-group">
					<div class="col-sm-10">
						<label class="control-label modal-label">EMAIL:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="EMAIL" value="<?php echo $row['EMAIL']; ?>">
					</div>
				</div>
           
            </div> 
			</div>
            <div class="modal-footer">
               	
						
                <button onclick="history.back(-1);" type="button" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                <button type="submit" name="edit" class="btn btn-success"><span class="glyphicon glyphicon-check"></span> Actualizar</a>
			</form>
                                               
                                        </div>
                                    </div>
                                    <!-- Page body end -->
                                </div>
                            </div>
                            <!-- Main-body end -->
                            <div id="styleSelector">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


   
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="assets/js/jquery/jquery.min.js "></script>
    <script type="text/javascript" src="assets/js/jquery-ui/jquery-ui.min.js "></script>
    <script type="text/javascript" src="assets/js/popper.js/popper.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/js/bootstrap.min.js "></script>
    <!-- waves js -->
    <script src="assets/pages/waves/js/waves.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="assets/js/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Custom js -->
    <script src="assets/js/pcoded.min.js"></script>
    <script src="assets/js/vertical/vertical-layout.min.js"></script>
    <script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
    
</body>

</html>







