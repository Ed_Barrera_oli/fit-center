<?php
	require '../table/conexion.php';
	
	$id = $_GET['id'];
	
	$sql = "SELECT * FROM documentos WHERE id = '$id'";
	$resultado = $mysqli->query($sql);
	$row = $resultado->fetch_array(MYSQLI_ASSOC);
	
?>
<!DOCTYPE html>
  <html lang="es">   <!-- Lenguaje de la pagina  -->
	<head>
	 <meta charset="UTF-8"> 
		<link href="../table/css/bootstrap.min.css" rel="stylesheet">
		<link href="../table/css/bootstrap-theme.css" rel="stylesheet">
		<script src="../table/js/jquery-3.1.1.min.js"></script>
		<script src="../table/js/bootstrap.min.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				$('.delete').click(function(){
					var parent = $(this).parent().attr('id');
					var service = $(this).parent().attr('data');
					var dataString = 'id='+service;
					$.ajax({
						type: "POST",
						url: "del_file.php",
						data: dataString,
						success: function() {			
							location.reload();
						}
					});
				});                 
			});    
		</script>
		
	</head>
	
<body>
		<div class="container">
			<div class="row">
				<h3 style="text-align:center">MODIFICAR REGISTRO</h3>
			</div>
			
			<form class="form-horizontal" method="POST" action="../table/update.php" enctype="multipart/form-data" autocomplete="off">
			<input type="hidden" id="id" name="id" value="<?php echo $row['id']; ?>" /> 
				
				<div class="form-group">
					<label for="direccion" class="col-sm-2 control-label">Dirección</label>
					<div class="col-sm-10">
						<select class="form-control" id="direccion" name="direccion">
                            <option value="DIRECCIÓN GENERAL"<?php if($row['direccion']=='DIRECCIÓN GENERAL') echo 'selected'; ?>>DIRECCIÓN GENERAL</option>
							<option value="SUBDIRECCIÓN GENERAL DE ADMINISTRACIÓN"<?php if($row['direccion']=='SUBDIRECCIÓN GENERAL DE ADMINISTRACIÓN') echo 'selected'; ?>>SUBDIRECCIÓN GENERAL DE ADMINISTRACIÓN</option>
							<option value="SUBDIRECCIÓN GENERAL DE BELLAS ARTES"<?php if($row['direccion']=='SUBDIRECCIÓN GENERAL DE BELLAS ARTES') echo 'selected'; ?>>SUBDIRECCIÓN GENERAL DE BELLAS ARTES</option>
                            <option value="SUBDIRECCIÓN GENERAL DE EDUCACIÓN ARTÍSTICA"<?php if($row['direccion']=='SUBDIRECCIÓN GENERAL DE EDUCACIÓN ARTÍSTICA') echo 'selected'; ?>>SUBDIRECCIÓN GENERAL DE EDUCACIÓN ARTÍSTICA</option>
                            <option value="SUBDIRECCIÓN GENERAL DEL PATRIMONIO INMUEBLE"<?php if($row['direccion']=='SUBDIRECCIÓN GENERAL DEL PATRIMONIO INMUEBLE') echo 'selected'; ?>>SUBDIRECCION GENERAL DEL PATRIMONIO INMUEBLE</option>
						</select>
					</div>
				</div>
                    
				<div class="form-group">
					<label for="area" class="col-sm-2 control-label">Area</label>
					<div class="col-sm-10">
						<select class="form-control" id="area" name="area">
						<!---DIRECCION GENERAL-->
<option value="DIRECCIÓN GENERAL"<?php if($row['area']=='DIRECCIÓN GENERAL') echo 'selected'; ?>>DIRECCIÓN GENERAL</option>
                        <!---SUBDIRECCIÓN GENERAL DE BELLAS ARTES-->
<option value="COMPAÑÍA NACIONAL DE DANZA"<?php if($row['area']=='COMPAÑÍA NACIONAL DE DANZA') echo 'selected'; ?>>COMPAÑÍA NACIONAL DE DANZA</option>
<option value="COMPAÑÍA NACIONAL DE ÓPERA"<?php if($row['area']=='COMPAÑÍA NACIONAL DE ÓPERA') echo 'selected'; ?>>COMPAÑÍA NACIONAL DE ÓPERA</option>
<option value="COORDINACIÓN NACIONAL DE DANZA"<?php if($row['area']=='COORDINACIÓN NACIONAL DE DANZA') echo 'selected'; ?>>COORDINACIÓN NACIONAL DE DANZA</option>
<option value="COORDINACIÓN NACIONAL DE LITERATURA"<?php if($row['area']=='COORDINACIÓN NACIONAL DE LITERATURA') echo 'selected'; ?>>COORDINACIÓN NACIONAL DE LITERATURA</option>
<option value="COORDINACIÓN NACIONAL DE MÚSICA Y ÓPERA"<?php if($row['area']=='COORDINACIÓN NACIONAL DE MÚSICA Y ÓPERA') echo 'selected'; ?>>COORDINACIÓN NACIONAL DE MÚSICA Y ÓPERA</option>
<option value="COORDINACIÓN NACIONAL DE TEATRO"<?php if($row['area']=='COORDINACIÓN NACIONAL DE TEATRO') echo 'selected'; ?>>COORDINACIÓN NACIONAL DE TEATRO</option>
<option value="COORDINACIÓN DEL PROYECTO DE EXTENSIÓN CULTURAL"<?php if($row['area']=='COORDINACIÓN DEL PROYECTO DE EXTENSIÓN CULTURAL') echo 'selected'; ?>>COORDINACIÓN DEL PROYECTO DE EXTENSIÓN CULTURAL</option>
<option value="DIRECCIÓN DEL CENTRO CULTURAL DEL BOSQUE"<?php if($row['area']=='DIRECCIÓN DEL CENTRO CULTURAL DEL BOSQUE') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO CULTURAL DEL BOSQUE</option>
<option value="GERENCIA DE LA ORQUESTA SINFÓNICA NACIONAL"<?php if($row['area']=='GERENCIA DE LA ORQUESTA SINFÓNICA NACIONAL') echo 'selected'; ?>>GERENCIA DE LA ORQUESTA SINFÓNICA NACIONAL</option>
<option value="GERENCIA DEL PALACIO DE BELLAS ARTES"<?php if($row['area']=='GERENCIA DEL PALACIO DE BELLAS ARTES') echo 'selected'; ?>>GERENCIA DEL PALACIO DE BELLAS ARTES</option>
<option value="COORDINACIÓN DE CONVENIOS"<?php if($row['area']=='COORDINACIÓN DE CONVENIOS') echo 'selected'; ?>>COORDINACIÓN DE CONVENIOS</option>
<option value="COORDINACIÓN DE DESARROLLO INSTITUCIONAL"<?php if($row['area']=='COORDINACIÓN DE DESARROLLO INSTITUCIONAL') echo 'selected'; ?>>COORDINACIÓN DE DESARROLLO INSTITUCIONAL
<option value="DEPARTAMENTO DE BECAS Y CONVENIOS"<?php if($row['area']=='DEPARTAMENTO DE BECAS Y CONVENIOS') echo 'selected'; ?>>DEPARTAMENTO DE BECAS Y CONVENIOS</option>
<option value="DIRECCIÓN DE ADMINISTRACIÓN"<?php if($row['area']=='DIRECCIÓN DE ADMINISTRACIÓN') echo 'selected'; ?>>DIRECCIÓN DE ADMINISTRACIÓN</option>
<option value="DIRECCIÓN DE ASUNTOS INTERNACIONALES"<?php if($row['area']=='DIRECCIÓN DE ASUNTOS INTERNACIONALES') echo 'selected'; ?>>DIRECCIÓN DE ASUNTOS INTERNACIONALES</option>
<option value="ORQUESTA DE CÁMARA DE BELLAS ARTES"<?php if($row['area']=='ORQUESTA DE CÁMARA DE BELLAS ARTES') echo 'selected'; ?>>ORQUESTA DE CÁMARA DE BELLAS ARTES</option>
<option value="SUBDIRECCIÓN DE PROGRAMAS ARTÍSTICOS"<?php if($row['area']=='SUBDIRECCIÓN DE PROGRAMAS ARTÍSTICOS') echo 'selected'; ?>>SUBDIRECCIÓN DE PROGRAMAS ARTÍSTICOS</option>
                          <!---SUBDIRECCIÓN GENERAL DE ADMINISTRACIÓN-->
<option value="DIRECCIÓN DE ASUNTOS JURIDICOS"<?php if($row['area']=='DIRECCIÓN DE ASUNTOS JURIDICOS') echo 'selected'; ?>>DIRECCIÓN DE ASUNTOS JURIDICOS</option>
<option value="DIRECCIÓN DE ASUNTOS LABORALES"<?php if($row['area']=='DIRECCIÓN DE ASUNTOS LABORALES') echo 'selected'; ?>>DIRECCIÓN DE ASUNTOS LABORALES</option>
<option value="DIRECCIÓN DE PERSONAL"<?php if($row['area']=='DIRECCIÓN DE PERSONAL') echo 'selected'; ?>>DIRECCIÓN DE PERSONAL</option>
<option value="DIRECCIÓN DE PROGRAMACIÓN Y PRESUPUESTO"<?php if($row['area']=='DIRECCIÓN DE PROGRAMACIÓN Y PRESUPUESTO') echo 'selected'; ?>>DIRECCIÓN DE PROGRAMACIÓN Y PRESUPUESTO</option>
<option value="DIRECCIÓN DE RECURSOS FINANCIEROS"<?php if($row['area']=='DIRECCIÓN DE RECURSOS FINANCIEROS') echo 'selected'; ?>>DIRECCIÓN DE RECURSOS FINANCIEROS</option>
<option value="DIRECCIÓN DE RECURSOS MATERIALES"<?php if($row['area']=='DIRECCIÓN DE RECURSOS MATERIALES') echo 'selected'; ?>>DIRECCIÓN DE RECURSOS MATERIALES</option>
<option value="DIRECCIÓN DE RECURSOS INFORMÁTICOS"<?php if($row['area']=='DIRECCIÓN DE RECURSOS INFORMÁTICOS') echo 'selected'; ?>>DIRECCIÓN DE RECURSOS INFORMÁTICOS</option>
<option value="TITULAR DE LA UNIDAD DE ENLACE"<?php if($row['area']=='TITULAR DE LA UNIDAD DE ENLACE') echo 'selected'; ?>>TITULAR DE LA UNIDAD DE ENLACE</option>
                           <!---SUBDIRECCIÓN GENERAL DE EDUCACIÓN ARTISTICA E INVESTIGACIÓN-->
<option value="CENTRO DE EDUCACIÓN ARTÍSTICA COLIMA JUAN RULFO"<?php if($row['area']=='CENTRO DE EDUCACIÓN ARTÍSTICA COLIMA JUAN RULFO') echo 'selected'; ?>>CENTRO DE EDUCACIÓN ARTÍSTICA COLIMA JUAN RULFO</option>
<option value="DIRECCIÓN DE ASUNTOS ACADÉMICOS"<?php if($row['area']=='DIRECCIÓN DE ASUNTOS ACADÉMICOS') echo 'selected'; ?>>DIRECCIÓN DE ASUNTOS ACADÉMICOS</option>
<option value="DIRECCIÓN DE CONSERVATORIO NACIONAL DE MÚSICA"<?php if($row['area']=='DIRECCIÓN DE CONSERVATORIO NACIONAL DE MÚSICA') echo 'selected'; ?>>DIRECCIÓN DE CONSERVATORIO NACIONAL DE MÚSICA</option>
<option value="DIRECCIÓN DE SERVICIOS EDUCATIVOS"<?php if($row['area']=='DIRECCIÓN DE SERVICIOS EDUCATIVOS') echo 'selected'; ?>>DIRECCIÓN DE SERVICIOS EDUCATIVOS</option>
<option value="DIRECCIÓN DE LA ACADEMIA DE DANZA MEXICANA"<?php if($row['area']=='DIRECCIÓN DE LA ACADEMIA DE DANZA MEXICANA') echo 'selected'; ?>>DIRECCIÓN DE LA ACADEMIA DE DANZA MEXICANA</option>
<option value="DIRECCIÓN DE LA ESCUELA NACIONAL DE ARTE TEATRAL"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA NACIONAL DE ARTE TEATRAL') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA NACIONAL DE ARTE TEATRAL</option>
<option value="DIRECCIÓN DE LA ESCUELA NACIONAL DE DANZA CLÁSICA Y CONTEMPORÁNEO"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA NACIONAL DE DANZA CLÁSICA Y CONTEMPORÁNEO') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA NACIONAL DE DANZA CLÁSICA Y CONTEMPORÁNEO</option>
<option value="DIRECCIÓN DE LA ESCUELA NACIONAL DE DANZA FOLKLÓRICA"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA NACIONAL DE DANZA FOLKLÓRICA') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA NACIONAL DE DANZA FOLKLÓRICA</option>
<option value="DIRECCIÓN DE LA ESCUELA NACIONAL DE DANZA NELLIE Y GLORIA CAMPOBELLO"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA NACIONAL DE DANZA NELLIE Y GLORIA CAMPOBELLO') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA NACIONAL DE DANZA NELLIE Y GLORIA CAMPOBELLO</option>
<option value="DIRECCIÓN DE LA ESCUELA SUPERIOR DE MÚSICA"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA SUPERIOR DE MÚSICA') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA SUPERIOR DE MÚSICA</option>
<option value="DIRECCIÓN DE LA ESCUELA SUPERIOR DE MÚSICA Y DANZA DE MONTERREY"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA SUPERIOR DE MÚSICA Y DANZA DE MONTERREY') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA SUPERIOR DE MÚSICA Y DANZA DE MONTERREY</option>
<option value="DIRECCIÓN DE LA ESCUELA DE ARTESANÍAS"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA DE ARTESANÍAS') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA DE ARTESANÍAS</option>
<option value="DIRECCIÓN DE LA ESCUELA DE DISEÑO"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA DE DISEÑO') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA DE DISEÑO</option>
<option value="DIRECCIÓN DE LA ESCUELA DE INICIACIÓN ARTISTICA No.1"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA DE INICIACIÓN ARTISTICA No.1') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA DE INICIACIÓN ARTISTICA No.1</option>
<option value="DIRECCIÓN DE LA ESCUELA DE INICIACIÓN ARTISTICA No.2"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA DE INICIACIÓN ARTISTICA No.2') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA DE INICIACIÓN ARTISTICA No.2</option>
<option value="DIRECCIÓN DE LA ESCUELA DE INICIACIÓN ARTISTICA No.3"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA DE INICIACIÓN ARTISTICA No.3') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA DE INICIACIÓN ARTISTICA No.3</option>
<option value="DIRECCIÓN DE LA ESCUELA DE INICIACIÓN ARTISTICA No.4"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA DE INICIACIÓN ARTISTICA No.4') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA DE INICIACIÓN ARTISTICA No.4</option>
<option value="DIRECCIÓN DE LA ESCUELA DE LAUDERÍA"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA DE LAUDERÍA') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA DE LAUDERÍA</option>
<option value="DIRECCIÓN DE LA ESCUELA DE PINTURA ESCULTURA Y GRABADO LA ESMERALDA"<?php if($row['area']=='DIRECCIÓN DE LA ESCUELA DE PINTURA ESCULTURA Y GRABADO LA ESMERALDA') echo 'selected'; ?>>DIRECCIÓN DE LA ESCUELA DE PINTURA ESCULTURA Y GRABADO LA ESMERALDA</option>
<option value="DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA CHIHUAHUA DAVID ALFARO SIQUEIROS"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA CHIHUAHUA DAVID ALFARO SIQUEIROS') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA CHIHUAHUA DAVID ALFARO SIQUEEROS</option>
<option value="DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA DIEGO RIVERA"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA DIEGO RIVERA') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTISTICA DIEGO RIVERA</option>
<option value="DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA FRIDA KAHLO"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA FRIDA KAHLO') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA FRIDA KAHLO</option>
<option value="DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA GUADALAJARA JOSÉ CLEMENTE OROZCO"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA GUADALAJARA JOSÉ CLEMENTE OROZCO') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA GUADALAJARA JOSÉ CLEMENTE OROZCO</option>
<option value="DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA HERMOSILLO JOSÉ EDUARDO PIERSON"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA HERMOSILLO JOSÉ EDUARDO PIERSON') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA HERMOSILLO JOSÉ EDUARDO PIERSON</option>
<option value="DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA LUIS SPOTA"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA LUIS SPOTA') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA LUIS SPOTA</option>
<option value="DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA MONTERREY ALFONSO REYES"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA MONTERREY ALFONSO REYES') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA MONTERREY ALFONSO REYES</option>
<option value="DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA MORELIA MIGUEL BERNAL JIMÉNEZ"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA MORELIA MIGUEL BERNAL JIMÉNEZ') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA MORELIA MIGUEL BERNAL JIMÉNEZ</option>
<option value="DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA MÉRIDA ERMILO ABREU GÓMEZ"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA MÉRIDA ERMILO ABREU GÓMEZ') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA MÉRIDA ERMILO ABREU GÓMEZ</option>                                                      
                            
<option value="DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA OAXACA MIGUEL CABRERA"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA OAXACA MIGUEL CABRERA') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA OAXACA MIGUEL CABRERA</option>
<option value="DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA QUERÉTARO IGNACIO MARIANO DE LAS CASAS"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA QUERÉTARO IGNACIO MARIANO DE LAS CASAS') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE EDUCACIÓN ARTÍSTICA QUERÉTARO IGNACIO MARIANO DE LAS CASAS</option>
<option value="DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN COREOGRÁFICA"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN COREOGRÁFICA') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN COREOGRÁFICA</option>
<option value="DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN DOCUMENTACIÓN E INFORMACIÓN MUSICAL CARLOS CHAVEZ"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN DOCUMENTACIÓN E INFORMACIÓN MUSICAL CARLOS CHAVEZ') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN DOCUMENTACIÓN E INFORMACIÓN MUSICAL CARLOS CHAVEZ</option>
<option value="DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN DOCUMENTACIÓN E INFORMACIÓN TEATRAL RODOLFO USIGLI"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN DOCUMENTACIÓN E INFORMACIÓN TEATRAL RODOLFO USIGLI') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN DOCUMENTACIÓN E INFORMACIÓN TEATRAL RODOLFO USIGLI</option>
<option value="DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN DOCUMENTACIÓN E INFORMACIÓN DE LA DANZA JOSÉ LIMÓN"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN DOCUMENTACIÓN E INFORMACIÓN DE LA DANZA JOSÉ LIMÓN') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN DOCUMENTACIÓN E INFORMACIÓN DE LA DANZA JOSÉ LIMÓN</option>
<option value="DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN E INFORMACIÓN DE ARTES PLASTICAS"<?php if($row['area']=='DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN E INFORMACIÓN DE ARTES PLASTICAS') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO DE INVESTIGACIÓN E INFORMACIÓN DE ARTES PLASTICAS</option>

<!---SUBDIRECCIÓN GENERAL DEL PATRIMONIO ARTÍSTICO INMUEBLE-->
<option value="COORDINÁCIÓN NACIONAL DE ARTES VISUALES"<?php if($row['area']=='COORDINÁCIÓN NACIONAL DE ARTES VISUALES') echo 'selected'; ?>>COORDINÁCIÓN NACIONAL DE ARTES VISUALES</option>
<option value="DIRECCIÓN DE ARQUITECTURA Y CONSERVACIÓN DEL PATRIMONIO ARTÍSTICO MUEBLE"<?php if($row['area']=='DIRECCIÓN DE ARQUITECTURA Y CONSERVACIÓN DEL PATRIMONIO ARTÍSTICO MUEBLE') echo 'selected'; ?>>DIRECCIÓN DE ARQUITECTURA Y CONSERVACIÓN DEL PATRIMONIO ARTÍSTICO MUEBLE</option>
<option value="DIRECCIÓN DE LA GALERÍA JOSÉ MARÍA VELASCO"<?php if($row['area']=='DIRECCIÓN DE LA GALERÍA JOSÉ MARÍA VELASCO') echo 'selected'; ?>>DIRECCIÓN DE LA GALERÍA JOSÉ MARÍA VELASCO</option>
<option value="DIRECCIÓN DE LA SALA DE ARTE PÚBLICO SIQUIEROS"<?php if($row['area']=='DIRECCIÓN DE LA SALA DE ARTE PÚBLICO SIQUIEROS') echo 'selected'; ?>>DIRECCIÓN DE LA SALA DE ARTE PÚBLICO SIQUIEROS</option>
<option value="DIRECCIÓN DEL CENTRO NACIONAL DE REGISTRO Y CONSERVACIÓN DEL PATRIMONIO ARTISTICO MUEBLE"<?php if($row['area']=='DIRECCIÓN DEL CENTRO NACIONAL DE REGISTRO Y CONSERVACIÓN DEL PATRIMONIO ARTISTICO MUEBLE') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO NACIONAL DE REGISTRO Y CONSERVACIÓN DEL PATRIMONIO ARTISTICO MUEBLE</option>
<option value="DIRECCIÓN DEL LABORATORIO ARTE ALAMEDA"<?php if($row['area']=='DIRECCIÓN DEL LABORATORIO ARTE ALAMEDA') echo 'selected'; ?>>DIRECCIÓN DEL LABORATORIO ARTE ALAMEDA</option>
<option value="DIRECCIÓN DEL MUSEO DIEGO RIVERA Y FRIDA KAHLO"<?php if($row['area']=='DIRECCIÓN DEL MUSEO DIEGO RIVERA Y FRIDA KAHLO') echo 'selected'; ?>>DIRECCIÓN DEL MUSEO DIEGO RIVERA Y FRIDA KAHLO</option>
<option value="DIRECCIÓN DEL MUSEO EX TERESA ARTE ACTUAL"<?php if($row['area']=='DIRECCIÓN DEL MUSEO EX TERESA ARTE ACTUAL') echo 'selected'; ?>>DIRECCIÓN DEL MUSEO EX TERESA ARTE ACTUAL</option>
<option value="DIRECCIÓN DEL MUSEO NACIONAL DE ARTE"<?php if($row['area']=='DIRECCIÓN DEL MUSEO NACIONAL DE ARTE') echo 'selected'; ?>>DIRECCIÓN DEL MUSEO NACIONAL DE ARTE</option>
<option value="DIRECCIÓN DEL MUSEO NACIONAL DE SAN CARLOS"<?php if($row['area']=='DIRECCIÓN DEL MUSEO NACIONAL DE SAN CARLOS') echo 'selected'; ?>>DIRECCIÓN DEL MUSEO NACIONAL DE SAN CARLOS</option>
<option value="DIRECCIÓN DEL MUSEO NACIONAL DE LA ESTAMPÁ"<?php if($row['area']=='DIRECCIÓN DEL MUSEO NACIONAL DE LA ESTAMPÁ') echo 'selected'; ?>>DIRECCIÓN DEL MUSEO NACIONAL DE LA ESTAMPÁ</option>
<option value="DIRECCIÓN DEL MUSEO DE ARTE ALVAR Y CARMEN T DE CARRILLO GIL"<?php if($row['area']=='DIRECCIÓN DEL MUSEO DE ARTE ALVAR Y CARMEN T DE CARRILLO GIL') echo 'selected'; ?>>DIRECCIÓN DEL MUSEO DE ARTE ALVAR Y CARMEN T DE CARRILLO GIL</option>
<option value="DIRECCIÓN DEL MUSEO DE ARTE CONTEMPORÁNEO INTERNACIONAL RUFINO TAMAYO"<?php if($row['area']=='DIRECCIÓN DEL MUSEO DE ARTE CONTEMPORÁNEO INTERNACIONAL RUFINO TAMAYO') echo 'selected'; ?><?php if($row['area']=='DIRECCIÓN DEL MUSEO DE ARTE CONTEMPORÁNEO INTERNACIONAL RUFINO TAMAYO') echo 'selected'; ?>>DIRECCIÓN DEL MUSEO DE ARTE CONTEMPORÁNEO INTERNACIONAL RUFINO TAMAYO</option>
<option value="DIRECCIÓN DEL MUSEO DE ARTE MODERNO"<?php if($row['area']=='DIRECCIÓN DEL MUSEO DE ARTE MODERNO') echo 'selected'; ?>>DIRECCIÓN DEL MUSEO DE ARTE MODERNO</option>
<option value="DIRECCIÓN DEL MUSEO DEL PALACIO DE BELLAS ARTES"<?php if($row['area']=='DIRECCIÓN DEL MUSEO DEL PALACIO DE BELLAS ARTES') echo 'selected'; ?>>DIRECCIÓN DEL MUSEO DEL PALACIO DE BELLAS ARTES</option>
<option value="DIRECCIÓN DE LA CASA DE ESTUDIO DE DAVID ALFARO SIQUIEROS LA TALLERA"<?php if($row['area']=='DIRECCIÓN DE LA CASA DE ESTUDIO DE DAVID ALFARO SIQUIEROS LA TALLERA') echo 'selected'; ?>>DIRECCIÓN DE LA CASA DE ESTUDIO DE DAVID ALFARO SIQUIEROS LA TALLERA</option>
<option value="DIRECCIÓN DEL CENTRO CULTURAL EL NIGROMANTE"<?php if($row['area']=='DIRECCIÓN DEL CENTRO CULTURAL EL NIGROMANTE') echo 'selected'; ?>>DIRECCIÓN DEL CENTRO CULTURAL EL NIGROMANTE</option>
<option value="DIRECCIÓN DEL MUSEO MURAL DIEGO RIVERA"<?php if($row['area']=='DIRECCIÓN DEL MUSEO MURAL DIEGO RIVERA') echo 'selected'; ?>>DIRECCIÓN DEL MUSEO MURAL DIEGO RIVERA</option>
<option value="DIRECCIÓN DEL MUSEO DE ARTE E HISTORIA DE CIUDAD JUAREZ"<?php if($row['area']=='DIRECCIÓN DEL MUSEO DE ARTE E HISTORIA DE CIUDAD JUAREZ') echo 'selected'; ?>>DIRECCIÓN DEL MUSEO DE ARTE E HISTORIA DE CIUDAD JUAREZ</option>
<option value="DIRECCIÓN DEL SALON DE PLÁSTICA MEXICANA I Y II"<?php if($row['area']=='DIRECCIÓN DEL SALON DE PLÁSTICA MEXICANA I Y II') echo 'selected'; ?>>DIRECCIÓN DEL SALON DE PLÁSTICA MEXICANA I Y II</option>
                  					</select>
					</div>
				</div>
				        
                
                   <div class="form-group">
					<label for="tipo" class="col-sm-2 control-label">TIPO</label>
					<div class="col-sm-10">
						<select class="form-control" id="tipo" name="tipo">
                            <option value="MANUAL DE FUNCIONES"<?php if($row['tipo']=='MANUAL DE FUNCIONES') echo 'selected'; ?>>MANUAL DE FUNCIONES</option>
							<option value="MANUAL DE PROCEDIMIENTOS"<?php if($row['tipo']=='MANUAL DE PROCEDIMINETOS') echo 'selected'; ?>>MANUAL DE PROCEDIMINETOS</option>
							<option value="GUÍA"<?php if($row['tipo']=='GUÍA') echo 'selected'; ?>>GUÍA</option>
                            <option value="LEY"<?php if($row['tipo']=='LEY') echo 'selected'; ?>>LEY</option>
                            <option value="OTRO"<?php if($row['tipo']=='OTRO') echo 'selected'; ?>>OTRO</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="nombre_archivo" class="col-sm-2 control-label">Nombre Archivo</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $row['nombre']; ?>" required>
					</div>
				</div>
					<div class="form-group">
					<label for="norma" class="col-sm-2 control-label">Estatus</label>
					<div class="col-sm-10">
						<select class="form-control" id="norma" name="norma">
       
		 <option value="REVISIÓN"<?php if($row['norma']=='REVISIÓN') echo 'selected'; ?>>REVISIÓN</option>
		 <option value="REVISADO" <?php if($row['norma']=='REVISADO') echo 'selected'; ?>>REVISADO</option>
	     <option value="CORRECIÓN"<?php if($row['norma']=='CORRECIÓN') echo 'selected'; ?>>CORRECIÓN</option>
         <option value="COMPLETO"<?php if($row['norma']=='COMPLETO') echo 'selected'; ?>>COMPLETO</option>
	     
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<a href="index.php" class="btn btn-default">Regresar</a>
                        <button type="submit" class="btn btn-primary">Guardar</button>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>